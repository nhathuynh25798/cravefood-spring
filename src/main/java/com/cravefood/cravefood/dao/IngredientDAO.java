package com.cravefood.cravefood.dao;

import com.cravefood.cravefood.model.Ingredient;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository(value = "ingredientDAO")
public class IngredientDAO extends AbstractDAO<Ingredient> {

    public List getListIngredient(int page) {
        return entityManager.createQuery("select DI from DishIngredient DI", Ingredient.class)
                .setFirstResult((page - 1) * 12).setMaxResults(12).getResultList();
    }
}
