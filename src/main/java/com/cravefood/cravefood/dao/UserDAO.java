package com.cravefood.cravefood.dao;

import com.cravefood.cravefood.dto.UserDTO;
import com.cravefood.cravefood.model.Address;
import com.cravefood.cravefood.model.Order;
import com.cravefood.cravefood.model.User;
import com.cravefood.cravefood.model.WishList;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository(value = "userDAO")
public class UserDAO extends AbstractDAO<User> {

    public boolean isEmailAddressUsed(String emailAddress) {
        DetachedCriteria c = DetachedCriteria.forClass(User.class).setProjection(Projections.rowCount())
                .add(Restrictions.eq("email", emailAddress));
        int res = findByCriteria(c).size();
        return res != 0;
    }

    public User getUserByEmailAddress(String emailAddress) {
        DetachedCriteria c = DetachedCriteria.forClass(User.class).setProjection(Projections.rowCount())
                .add(Restrictions.eq("email", emailAddress));
        return (User) findByCriteria(c).get(0);
    }

    public List<WishList> getWishLists(Integer id) {
        return entityManager.createQuery("select W from WishList W where W.createBy.id = :id", WishList.class)
                .setParameter("id", id).getResultList();
    }

    public List<Address> getAddresses(Integer id) {
        return entityManager.createQuery("select A from Address A where A.createBy = :id", Address.class)
                .setParameter("id", id).getResultList();
    }

    public List<Order> getOrders(Integer id) {
        return entityManager.createQuery("select O from Order O where O.createBy = :id", Order.class)
                .setParameter("id", id).getResultList();
    }

    public List<UserDTO> fetchUsers(int page) {
        List<UserDTO> userDTOs = new ArrayList<>();
        List<User> users = entityManager.createQuery("select U from User U where U.role.id = 2", User.class)
                .setFirstResult((page - 1) * 12).setMaxResults(12).getResultList();
        if(users != null && !users.isEmpty()) {
            for (User user: users) {
                userDTOs.add(UserDTO.build(user));
            }
        }
        return userDTOs;
    }
}