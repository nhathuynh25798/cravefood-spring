package com.cravefood.cravefood.dao;

import com.cravefood.cravefood.model.OrderItem;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository(value = "orderItemDAO")
@Transactional(rollbackFor = Exception.class)
public class OrderItemDAO {

    @PersistenceContext
    protected EntityManager entityManager;

    public void merge(OrderItem model) {
        try {
            entityManager.merge(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(OrderItem model) {
        try {
            entityManager.remove(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
