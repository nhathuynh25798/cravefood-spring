package com.cravefood.cravefood.dao;

import com.cravefood.cravefood.dto.WishListRequest;
import com.cravefood.cravefood.model.Product;
import com.cravefood.cravefood.model.User;
import com.cravefood.cravefood.model.WishList;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Repository(value = "wishListDAO")
@Transactional(rollbackFor = Exception.class)
public class WishListDAO {

    @PersistenceContext
    protected EntityManager entityManager;

    public WishList createWishList(WishListRequest model) {
        User user = entityManager.find(User.class, model.getUserId());
        Product product = entityManager.find(Product.class, model.getProductId());
        WishList wishList = new WishList();
        wishList.setCreateBy(user);
        wishList.setProduct(product);
        wishList.setEnable(1);
        return wishList;
    }

    public void saveWishList(WishListRequest model) {
        try {
            entityManager.merge(createWishList(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteWishList( WishListRequest model) {
        try {
            WishList wishList = createWishList(model);
            entityManager.remove(entityManager.contains(wishList) ? entityManager : entityManager.merge(wishList));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void remove( WishList wishList) {
        try {
            entityManager.remove(entityManager.contains(wishList) ? entityManager : entityManager.merge(wishList));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<WishList> getWishListByProductId(int productId) {
        try {
            return entityManager.createQuery("select W from WishList W where WishList .product.id = :id", WishList.class)
                    .setParameter("id", productId).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

}
