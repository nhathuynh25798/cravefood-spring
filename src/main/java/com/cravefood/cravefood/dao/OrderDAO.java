package com.cravefood.cravefood.dao;

import com.cravefood.cravefood.model.Order;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository(value = "orderDAO")
public class OrderDAO extends AbstractDAO<Order> {

    public Integer getNextId() {
        return ((BigInteger) entityManager.createNativeQuery("SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'cravefood' AND TABLE_NAME = '`order`'").getResultList().get(0)).intValue();
    }

    public Order saveOrder(Order model) {
        try {
            Order order = entityManager.merge(model);
            entityManager.flush();
            return order;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteOrder(Order model) {
        try {
            entityManager.remove(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
