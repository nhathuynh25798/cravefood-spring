package com.cravefood.cravefood.dao;

import com.cravefood.cravefood.model.Province;
import org.springframework.stereotype.Repository;

@Repository(value = "provinceDAO")
public class ProvinceDAO extends AbstractDAO<Province> {
}
