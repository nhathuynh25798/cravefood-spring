package com.cravefood.cravefood.dao;

import com.cravefood.cravefood.model.AbstractModel;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional(rollbackFor = Exception.class)
public abstract class AbstractDAO<T extends AbstractModel> {

    @PersistenceContext
    protected EntityManager entityManager;

    public T getById(Class<T> clazz, Integer id) {
        return getCurrentSession().get(clazz, id);
    }

    public List getAll(Class<T> clazz) {
        return entityManager.createQuery("SELECT T FROM " + clazz.getName() + " T").getResultList();
    }

    public List<T> getInPage(Class<T> clazz, int page) {
        return entityManager.createQuery("SELECT T FROM " + clazz.getName() + " T").setFirstResult((page - 1) * 12).setMaxResults(12).getResultList();
    }

    public int getRowCount(Class<T> clazz) {
        return getAll(clazz).size();
    }

    public T getBySlug(Class<T> clazz, String slug) {
        List<T> res = findByCriteria(DetachedCriteria.forClass(clazz).setProjection(Projections.rowCount())
                .add(Restrictions.eq("slug", slug)));
        if(!res.isEmpty()) return res.get(0);
        return null;
    }

    public List findByName(Class<T> clazz, String name) {
        return entityManager.createQuery("SELECT T FROM " + clazz.getName() + " T where T.name like '%" + name + "%'").getResultList();
    }

    public void save(T model) {
        try {
            entityManager.persist(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void merge(T model) {
        try {
            entityManager.merge(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(T model) {
        try {
            entityManager.remove(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("rawtypes")
    public List findByCriteria(DetachedCriteria criteria) {
        return criteria.getExecutableCriteria(getCurrentSession()).setProjection(null)
                .setResultTransformer(CriteriaSpecification.ROOT_ENTITY).list();
    }

    public Session getCurrentSession() {
        return entityManager.unwrap(Session.class);
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public boolean isStringUsed(Class<T> clazz, String attribute, String string) {
        DetachedCriteria c = DetachedCriteria.forClass(clazz).setProjection(Projections.rowCount())
                .add(Restrictions.eq(attribute, string));
        int res = findByCriteria(c).size();
        return res != 0;
    }
}
