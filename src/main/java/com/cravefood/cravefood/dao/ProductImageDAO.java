package com.cravefood.cravefood.dao;

import com.cravefood.cravefood.model.ProductImage;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository(value = "productImageDAO")
@Transactional(rollbackFor = Exception.class)
public class ProductImageDAO {

    @PersistenceContext
    public EntityManager entityManager;



    public void addProductImage(ProductImage model) {
        try {
            entityManager.merge(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeProductImage(ProductImage model) {
        try {
            entityManager.remove(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ProductImage getById(int id) {
        return getCurrentSession().get(ProductImage.class, id);
    }

    public Session getCurrentSession() {
        return entityManager.unwrap(Session.class);
    }
}
