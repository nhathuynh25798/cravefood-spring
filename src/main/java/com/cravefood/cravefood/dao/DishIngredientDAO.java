package com.cravefood.cravefood.dao;

import com.cravefood.cravefood.model.DishIngredient;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository(value = "dishIngredientDAO")
@Transactional(rollbackFor = Exception.class)
public class DishIngredientDAO {

    @PersistenceContext
    protected EntityManager entityManager;

    public void addDishIngredient(DishIngredient ingredient) {
        try {
            entityManager.merge(ingredient);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void deleteDishIngredient(DishIngredient ingredient) {
        try {
            entityManager.merge(ingredient);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
