package com.cravefood.cravefood.dao;

import com.cravefood.cravefood.model.Product;
import com.cravefood.cravefood.model.User;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;
import java.util.Properties;

@Repository(value = "productDAO")
public class ProductDAO extends AbstractDAO<Product> {

    public List<Product> getAllProduct() {
        return getAll(Product.class);
    }

    public Integer getNextId() {
        return ((BigInteger) entityManager.createNativeQuery("SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'cravefood' AND TABLE_NAME = 'product'").getResultList().get(0)).intValue();
    }

    public List<Product> findByName(String name) {
        return findByName(Product.class, name);
    }

    public Product saveProduct(Product product) {
        try {
            Product productSaved = entityManager.merge(product);
            entityManager.flush();
            return productSaved;
        } catch (Exception exception) {
            exception.printStackTrace();
            return null;
        }
    }
}