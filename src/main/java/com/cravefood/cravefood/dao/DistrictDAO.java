package com.cravefood.cravefood.dao;

import com.cravefood.cravefood.model.District;
import com.cravefood.cravefood.model.Ward;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository(value = "districtDAO")
public class DistrictDAO extends AbstractDAO<District> {

    public List getDistrictsByProvinceId(Integer provinceId) {
        return entityManager.createQuery("Select d from District d where d.province.id = " + provinceId, District.class).getResultList();
    }
}
