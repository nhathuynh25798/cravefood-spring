package com.cravefood.cravefood.dao;

import com.cravefood.cravefood.model.Ward;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository(value = "wardDAO")
public class WardDAO extends AbstractDAO<Ward> {

    public List getWardsByDistrictId(Integer districtId) {
        return entityManager.createQuery("Select w from Ward w where w.district.id = " + districtId, Ward.class).getResultList();
    }
}
