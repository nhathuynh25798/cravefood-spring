package com.cravefood.cravefood.dao;

import com.cravefood.cravefood.model.Address;
import org.springframework.stereotype.Repository;

@Repository(value = "addressDAO")
public class AddressDAO extends AbstractDAO<Address> {
}
