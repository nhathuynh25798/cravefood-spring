package com.cravefood.cravefood.dao;

import com.cravefood.cravefood.model.Size;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository(value = "sizeDAO")
@Transactional(rollbackFor = Exception.class)
public class SizeDAO extends AbstractDAO<Size> {

}
