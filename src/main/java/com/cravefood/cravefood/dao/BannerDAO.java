package com.cravefood.cravefood.dao;

import com.cravefood.cravefood.model.Banner;
import org.springframework.stereotype.Repository;

@Repository(value = "bannerDAO")
public class BannerDAO extends AbstractDAO<Banner> {
}
