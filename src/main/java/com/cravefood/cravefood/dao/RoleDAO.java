package com.cravefood.cravefood.dao;

import com.cravefood.cravefood.model.Role;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository(value = "roleDAO")
public class RoleDAO {

    @PersistenceContext
    protected EntityManager entityManager;

    public Role getById(Integer id) {
        return entityManager.unwrap(Session.class).get(Role.class, id);
    }
}
