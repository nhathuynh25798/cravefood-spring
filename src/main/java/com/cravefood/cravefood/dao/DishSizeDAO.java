package com.cravefood.cravefood.dao;

import com.cravefood.cravefood.model.DishSize;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository(value = "dishSizeDAO")
@Transactional(rollbackFor = Exception.class)
public class DishSizeDAO {

    @PersistenceContext
    protected EntityManager entityManager;

    public DishSize getDishSize(int productId, int sizeId) {
        return entityManager.createQuery("select D from DishSize D where D.product.id = :productId and D.size.id = :sizeId", DishSize.class)
                .setParameter("productId", productId).setParameter("sizeId", sizeId).getResultList().get(0);
    }

    public void addDishSize(DishSize model) {
        try {
            entityManager.merge(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeDishSize(DishSize dishSize) {
        try {
            entityManager.remove(entityManager.contains(dishSize) ? entityManager : entityManager.merge(dishSize));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
