package com.cravefood.cravefood.dao;

import com.cravefood.cravefood.model.Category;
import com.cravefood.cravefood.model.Product;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository(value = "categoryDAO")
public class CategoryDAO extends AbstractDAO<Category> {

    public List<Category> getAllCategoryLimitProduct() {
        List<Category> list = getAll(Category.class);
        for (Category c : list) {
            List<Product> products = entityManager.createQuery("select P from Product P join P.categories C  where C.id = :id", Product.class).setParameter("id", c.getId()).setMaxResults(10).getResultList();
            Set<Product> res = new HashSet<>(products);
            c.setProducts(res);
        }
        return list;
    }

    public Category getCategoryBySlugLimitProduct(String slug) {
        Category c = getBySlug(Category.class, slug);
        List<Product> products = entityManager.createQuery("select P from Product P join P.categories C  where C.slug = :slug", Product.class).setParameter("slug", slug).setMaxResults(10).getResultList();
        Set<Product> res = new HashSet<>(products);
        c.setProducts(res);
        return c;
    }

    public List<Product> getProductsByCategorySlug(String slug) {
        return entityManager.createQuery("select P from Product P join P.categories C  where C.slug = :slug", Product.class)
                .setParameter("slug", slug).getResultList();
    }

    public List<Category> getCategoriesShowHome() {
        return entityManager.createQuery("select C from Category C where C.showHome = 1").setMaxResults(10).getResultList();
    }

    public List<Category> getCategoriesShowMenu() {
        return entityManager.createQuery("select C from Category C where C.showMenu = 1").getResultList();
    }
}
