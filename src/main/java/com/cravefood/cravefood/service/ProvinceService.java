package com.cravefood.cravefood.service;

import com.cravefood.cravefood.dao.DishIngredientDAO;
import com.cravefood.cravefood.dao.DishSizeDAO;
import com.cravefood.cravefood.dao.ProductImageDAO;
import com.cravefood.cravefood.dao.ProvinceDAO;
import com.cravefood.cravefood.model.Province;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ProvinceService {

    private final ProvinceDAO provinceDAO;

    @Autowired
    public ProvinceService(ProvinceDAO provinceDAO, ProductImageDAO productImageDAO, DishSizeDAO dishSizeDAO, DishIngredientDAO ingredientDAO) {
        this.provinceDAO = provinceDAO;
    }

    public List getAllProvince() {
        return provinceDAO.getAll(Province.class);
    }
}
