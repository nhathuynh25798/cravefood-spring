package com.cravefood.cravefood.service;

import com.cravefood.cravefood.dao.DishSizeDAO;
import com.cravefood.cravefood.dao.ProductDAO;
import com.cravefood.cravefood.dao.UserDAO;
import com.cravefood.cravefood.dto.*;
import com.cravefood.cravefood.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class UserService implements UserDetailsService {

    private final UserDAO userDAO;
    private final ProductDAO productDAO;
    private final DishSizeDAO dishSizeDAO;
    private final WishListService wishListService;
    private final AddressService addressService;
    private final OrderService orderService;

    @Autowired
    public UserService(UserDAO userDAO, ProductDAO productDAO, DishSizeDAO dishSizeDAO, WishListService wishListService, AddressService addressService, OrderService orderService) {
        this.userDAO = userDAO;
        this.productDAO = productDAO;
        this.dishSizeDAO = dishSizeDAO;
        this.wishListService = wishListService;
        this.addressService = addressService;
        this.orderService = orderService;
    }

    public User getUserById(Integer id) {
        return userDAO.getById(User.class, id);
    }

    public List<UserDTO> getAllUsers(int page) {
        List<UserDTO> userDTOs = new ArrayList<>();
        for(User user : userDAO.getInPage(User.class, page)) {
            userDTOs.add(UserDTO.build(user));
        }
        return userDTOs;
    }

    public int getRowCount() {return userDAO.getRowCount(User.class); }

    public void deleteUser(Integer id) {
        userDAO.delete(getUserById(id));
    }

    public List<WishListDTO> getWishList(Integer id) {
        List<WishListDTO> wishLists = new ArrayList<>();
        for (WishList wishList : userDAO.getWishLists(id)) {
            wishLists.add(WishListDTO.build(wishList));
        }
        return wishLists;
    }

    public List<AddressDTO> getAddresses(Integer id) {
        List<AddressDTO> addressDTOs = new ArrayList<>();
        for (Address address : userDAO.getAddresses(id)) {
            addressDTOs.add(AddressDTO.build(address));
        }
        return addressDTOs;
    }

    public List<OrderDTO> getOrders(Integer id) {
        List<OrderDTO> orderDTOs = new ArrayList<>();
        for (Order order : userDAO.getOrders(id)) {
            OrderDTO orderDTO = OrderDTO.build(order);
            List<OrderItem> orderItems = new ArrayList<>(order.getOrderItems());
            List<OrderItemDTO> orderItemDTOs = new ArrayList<>();
            TreeMap<Integer, List<OrderItem>> map = new TreeMap<>();
            for (OrderItem item : orderItems) {
                if (map.containsKey(item.getProductId())) {
                    map.get(item.getProductId()).add(item);
                } else {
                    List<OrderItem> sameProduct = new ArrayList<>();
                    sameProduct.add(item);
                    map.put(item.getProductId(), sameProduct);
                }
            }
            for (Map.Entry<Integer, List<OrderItem>> entry : map.entrySet()) {
                Product product = productDAO.getById(Product.class, entry.getKey());
                OrderItemDTO orderItemDTO = OrderItemDTO.build(product);
                for (OrderItem item : entry.getValue()) {
                    DishSize dishSize = dishSizeDAO.getDishSize(item.getProductId(), item.getSizeId());
                    orderItemDTO.getSizes().add(OrderItemQuantityDTO.build(dishSize, item));
                }
                orderItemDTOs.add(orderItemDTO);
            }
            orderDTO.setItems(orderItemDTOs);
            orderDTOs.add(orderDTO);
        }
        return orderDTOs;
    }

    public void addWishList(WishListRequest wishList) {
        wishListService.saveWishList(wishList);
    }

    public void deleteWishList(WishListRequest wishList) {
        wishListService.deleteWishList(wishList);
    }

    public void addOrder(OrderRequest orderRequest) {
        Order order = new Order();
        order.setCreateBy(orderRequest.getUserId());
        order.setName(orderRequest.getName());
        order.setAddressDetail(orderRequest.getDetail());
        order.setWard(orderRequest.getWard());
        order.setDistrict(orderRequest.getDistrict());
        order.setProvince(orderRequest.getProvince());
        order.setAddressPhoneContact(orderRequest.getPhone());
        if (!orderRequest.getNote().equalsIgnoreCase("") || orderRequest.getNote() != null) {
            order.setNote(orderRequest.getNote());
        }
        if (!orderRequest.getAddressNote().equalsIgnoreCase("") || orderRequest.getAddressNote() != null) {
            order.setAddressNote(orderRequest.getAddressNote());
        }
        order.setDeliveryDate(orderRequest.getDeliveryDate().replace('-', '/'));
        order.setCodeString(orderRequest.getCodeString());
        order.setTotal(orderRequest.getTotal());
        order.setEnable(orderRequest.getEnable());
        order.setStatus(orderRequest.getStatus());
        order.setCreatedAt(new Date());
        order.setTotalPrice(orderRequest.getCartTotalDiscountPrice());
        order.setTotalOriginPrice(orderRequest.getCartTotalDiscountPrice());
        Order orderSaved = orderService.saveOrder(order);
        for (OrderItemRequest item : orderRequest.getOrderItemRequest()) {
            OrderItem orderItem = new OrderItem();
            orderItem.setOrder(orderSaved);
            orderItem.setProductId(item.getProductId());
            orderItem.setSizeId(item.getSizeId());
            orderItem.setQuantity(item.getQuantity());
            orderService.saveOrderItem(orderItem);
        }
    }

    public void deleteOrder(int id) {
        orderService.deleteOrder(id);
    }

    public void addAddress(List<Address> addresses) {
        for (Address address : addresses) {
            if(address.getCreatedAt() == null) {
                address.setCreatedAt(new Date());
            }
            addressService.addAddress(address);
        }
    }

    public ResponseEntity<?> deleteAddress(Integer id) {
        Address address = addressService.getAddressById(id);
        if (address.getDefaultAddress() == 1) {
            return ResponseEntity.badRequest().body(new MessageResponse("address default not delete"));
        } else {
            addressService.deleteAddress(id);
            return ResponseEntity.ok(new MessageResponse("Xoá địa chỉ thành công"));
        }
    }

    public UserDTO getUser(Integer id) {
        return UserDTO.build(userDAO.getById(User.class, id));
    }

    public void updateUser(UpdateUserRequestDTO userDTO) {
        User user = userDAO.getById(User.class, userDTO.getId());
        user.setName(userDTO.getName());
        user.setPhoneNumber(userDTO.getPhone());
        user.setGender(userDTO.getGender());
        user.setBirthDay(userDTO.getBirthday().replace('-', '/'));
        userDAO.merge(user);
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User res = userDAO.getUserByEmailAddress(email);
        return UserDTO.build(res);
    }

    public List<UserDTO> fetchUser(int page) {
        return userDAO.fetchUsers(page);
    }
}
