package com.cravefood.cravefood.service;

import com.cravefood.cravefood.dao.CategoryDAO;
import com.cravefood.cravefood.dto.CategoriesDTO;
import com.cravefood.cravefood.dto.MenuDTO;
import com.cravefood.cravefood.dto.MessageResponse;
import com.cravefood.cravefood.dto.ProductsItemDTO;
import com.cravefood.cravefood.model.Category;
import com.cravefood.cravefood.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class CategoryService {

    private final CategoryDAO categoryDAO;

    @Autowired
    public CategoryService(CategoryDAO categoryDAO) {
        this.categoryDAO = categoryDAO;
    }

    public CategoriesDTO getCategoryBySlugLimitProduct(String slug) {
        return CategoriesDTO.build(categoryDAO.getCategoryBySlugLimitProduct(slug));
    }

    public List getAllCategories(int page) {
        List<CategoriesDTO> categoriesDTOs = new ArrayList<>();
        for (Category category :
                categoryDAO.getInPage(Category.class, page)) {
            categoriesDTOs.add(CategoriesDTO.build(category));
        }
        return categoriesDTOs;
    }

    public List<ProductsItemDTO> getProductsByCategorySlug(String slug) {
        List<ProductsItemDTO> productsItemDTO = new ArrayList<>();
        for (Product product :
                categoryDAO.getProductsByCategorySlug(slug)) {
            productsItemDTO.add(ProductsItemDTO.build(product));
        }
        return productsItemDTO;
    }

    public List<CategoriesDTO> getCategoriesShowHome() {
        List<CategoriesDTO> categoriesDTOs = new ArrayList<>();
        for (Category category :
                categoryDAO.getCategoriesShowHome()
        ) {
            categoriesDTOs.add(CategoriesDTO.build(category));
        }
        return categoriesDTOs;
    }

    public List<MenuDTO> getCategoriesShowMenu() {
        List<MenuDTO> menuDTOs = new ArrayList<>();
        for (Category category :
                categoryDAO.getCategoriesShowMenu()
        ) {
            menuDTOs.add(MenuDTO.build(category));
        }
        return menuDTOs;
    }

    public List<MenuDTO> adminGetCategories() {
        List<MenuDTO> menuDTOs = new ArrayList<>();
        List<Category> categories = categoryDAO.getAll(Category.class);
        for (Category category : categories) {
            menuDTOs.add(MenuDTO.build(category));
        }
        return menuDTOs;
    }

    public ResponseEntity<?> addCategory(CategoriesDTO category) {
        try {
            Category categoryNeedSave = new Category();
            if (category.getId() >= 1) {
                categoryNeedSave.setId(category.getId());
            }
            if (category.getSlug() != null) {
                if (category.getId() <= 0 && categoryDAO.isStringUsed(Category.class, "slug", category.getSlug())) {
                    return ResponseEntity
                            .badRequest()
                            .body(new MessageResponse("slug is used", 401));
                } else {
                    categoryNeedSave.setSlug(category.getSlug());
                }
            } else {
                return ResponseEntity
                        .badRequest()
                        .body(new MessageResponse("this product is not slug", 401));
            }
            if (category.getName() != null) {
                if (category.getId() <= 0 && categoryDAO.isStringUsed(Category.class, "name", category.getSlug())) {
                    return ResponseEntity
                            .badRequest()
                            .body(new MessageResponse("name is used", 401));
                } else {
                    categoryNeedSave.setName(category.getName());
                }
            } else {
                return ResponseEntity
                        .badRequest()
                        .body(new MessageResponse("this product is not name", 401));
            }
            if (category.getCreateBy() < 0) {
                categoryNeedSave.setCreateBy(category.getCreateBy());
            } else {
                categoryNeedSave.setCreateBy(1);
            }
            categoryNeedSave.setShowHome(category.getShowHome());
            categoryNeedSave.setEnable(category.getEnable());
            categoryNeedSave.setCreatedAt(new Date());
            categoryNeedSave.setShowMenu(category.getShowMenu());
            categoryDAO.merge(categoryNeedSave);
            return ResponseEntity.ok(new MessageResponse("Lưu danh mục sản phẩm thành công!", 200));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("DEFAULT", 401));
        }
    }

    public void deleteCategory(int id) {
        Category category = categoryDAO.getById(Category.class, id);
        categoryDAO.delete(category);
    }

    public int getCountRow() {
        return categoryDAO.getRowCount(Category.class);
    }

    public List<CategoriesDTO> getAll() {
        List<CategoriesDTO> categoriesDTOs = new ArrayList<>();
        List<Category> categories = categoryDAO.getAll(Category.class);
        for (Category category : categories) {
            categoriesDTOs.add(CategoriesDTO.build(category));
        }
        return categoriesDTOs;
    }
}
