package com.cravefood.cravefood.service;

import com.cravefood.cravefood.controller.ProductCategoryDAO;
import com.cravefood.cravefood.dao.*;
import com.cravefood.cravefood.dto.*;
import com.cravefood.cravefood.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ResourceUtils;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class ProductService {

    private final ProductDAO productDAO;
    private final ProductImageDAO productImageDAO;
    private final CategoryDAO categoryDAO;
    private final ProductCategoryDAO productCategoryDAO;
    private final DishSizeService dishSizeService;
    private final SizeDAO sizeDAO;
    private final IngredientDAO ingredientDAO;
    private final WishListDAO wishListDAO;

    @Autowired
    public ProductService(ProductDAO productDAO, ProductImageDAO productImageDAO, CategoryDAO categoryDAO, ProductCategoryDAO productCategoryDAO, DishSizeService dishSizeService, SizeDAO sizeDAO, IngredientDAO ingredientDAO, WishListDAO wishListDAO) {
        this.productDAO = productDAO;
        this.productImageDAO = productImageDAO;
        this.categoryDAO = categoryDAO;
        this.productCategoryDAO = productCategoryDAO;
        this.dishSizeService = dishSizeService;
        this.sizeDAO = sizeDAO;
        this.ingredientDAO = ingredientDAO;
        this.wishListDAO = wishListDAO;
    }

    public ProductDTO getProductBySlug(String slug) {
        return ProductDTO.build(productDAO.getBySlug(Product.class, slug));
    }

    public List getAllProducts(int page) {
        List<ProductsItemDTO> productDTOs = new ArrayList<>();
        for (Product product :
                productDAO.getInPage(Product.class, page)) {
            productDTOs.add(ProductsItemDTO.build(product));
        }
        return productDTOs;
    }

    public List findByName(String name) {
        List<ProductDTO> productDTOs = new ArrayList<>();
        for (Product product :
                productDAO.findByName(name)) {
            productDTOs.add(ProductDTO.build(product));
        }
        return productDTOs;
    }

    public Integer getTheNextId() {
        return productDAO.getNextId();
    }

    public ResponseEntity<?> saveProduct(ProductDTO product) {
        try {
            Product productNeedSave = new Product();
            if (product.getId() > 0) {
                productNeedSave.setId(product.getId());
            }
            if (product.getName() != null) {
                productNeedSave.setName(product.getName());
            }
            if (product.getCreatedBy() > 0) {
                productNeedSave.setCreateBy(product.getCreatedBy());
            } else {
                productNeedSave.setCreateBy(1);
            }
            if (product.getEnable() >= 0) {
                productNeedSave.setEnable(product.getEnable());
            } else {
                productNeedSave.setEnable(1);
            }
            if (product.getDescription() != null) {
                productNeedSave.setDescription(product.getDescription());
            }
            if (product.getDiscount() > 0) {
                productNeedSave.setDiscount(product.getDiscount());
            }
            if (product.getSlug() != null) {
                if (product.getId() <= 0 && productDAO.isStringUsed(Product.class, "slug", product.getSlug())) {
                    return ResponseEntity
                            .badRequest()
                            .body(new MessageResponse("slug is used", 401));
                } else {
                    productNeedSave.setSlug(product.getSlug());
                }
            } else {
                return ResponseEntity
                        .badRequest()
                        .body(new MessageResponse("this product is not slug", 401));
            }
            productNeedSave.setCreatedAt(new Date());
            Product productSaved = productDAO.saveProduct(productNeedSave);
            if(product.getStringBase64Files() != null && product.getStringBase64Files().size() != 0) {
                for (StringBase64File itemFile: product.getStringBase64Files()) {
                    uploadProductImage(itemFile);
                    ProductImage productImage = new ProductImage();
                    productImage.setProduct(productSaved);
                    productImage.setContent("/images/" + itemFile.getName());
                    productImageDAO.addProductImage(productImage);
                }
            }
            if(product.getCategories() != null && product.getCategories().size() != 0) {
                for(MenuDTO item : product.getCategories()) {
                    ProductCategory productCategory = new ProductCategory();
                    Category category = categoryDAO.getById(Category.class, item.getId());
                    productCategory.setProduct(productSaved);
                    productCategory.setCategory(category);
                    productCategoryDAO.addProductCategory(productCategory);
                }
            }
            if(product.getImages() != null && product.getImages().size() != 0) {
                for(ProductImageDTO productImageDTO: product.getImages()) {
                    ProductImage productImage = productImageDAO.getById(productImageDTO.getId());
                    productImageDAO.addProductImage(productImage);
                }
            }
            if(product.getSizes() != null && product.getSizes().size() != 0) {
                for(DishSizeDTO dishSizeDTO : product.getSizes()) {
                    dishSizeService.addDishSize(dishSizeDTO, productSaved);
                }
            }
            return ResponseEntity.ok(new MessageResponse("Lưu sản phẩm thành công!", 200));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("DEFAULT", 401));
        }
    }

    public void uploadProductImage(StringBase64File image) throws IOException {
        String[] strings = image.getFile().split(",");
        byte[] data = DatatypeConverter.parseBase64Binary(strings[1]);
        URL s = ResourceUtils.getURL("classpath:static/");
        String path = s.getPath() + "/images/" + image.getName();
        File file = new File(path);
        OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file));
        outputStream.write(data);
    }

    public int getCountRow() {
        return productDAO.getRowCount(Product.class);
    }

    public void deleteDishSize(DishSizeNotIngredientsDTO dishSizeNotIngredientsDTO) {
        Product product = productDAO.getById(Product.class, dishSizeNotIngredientsDTO.getProductId());
        Size size = sizeDAO.getById(Size.class, dishSizeNotIngredientsDTO.getSizeId());
        DishSize dishSize = new DishSize();
        dishSize.setSize(size);
        dishSize.setProduct(product);
        dishSizeService.removeDishSize(dishSize);
    }

    public void deleteDishIngredient(DishIngredientRequest dishIngredientRequest) {
        Product product = productDAO.getById(Product.class, dishIngredientRequest.getProductId());
        Size size = sizeDAO.getById(Size.class, dishIngredientRequest.getSizeId());
        Ingredient ingredient = ingredientDAO.getById(Ingredient.class, dishIngredientRequest.getIngredientId());
        DishIngredient dishIngredient = new DishIngredient();
        dishIngredient.setIngredient(ingredient);
        dishIngredient.setProduct(product);
        dishIngredient.setSize(size);
        dishSizeService.removeDishIngredient(dishIngredient);
    }

    public void deleteProduct(Integer id) {
        try {
            Product product = productDAO.getById(Product.class, id);
            URL s = ResourceUtils.getURL("classpath:static/");
            if(product.getImages() != null && !product.getImages().isEmpty()) {
                for (ProductImage image : product.getImages()) {
                    String path = s.getPath() + image.getContent();
                    File file = new File(path);
                    if(file.delete()) {
                        ProductImage productImage = productImageDAO.getById(image.getId());
                        productImageDAO.removeProductImage(productImage);
                    }
                }
            }
            productDAO.delete(product);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
