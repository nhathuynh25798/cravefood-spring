package com.cravefood.cravefood.service;

import com.cravefood.cravefood.dao.DishIngredientDAO;
import com.cravefood.cravefood.dao.DishSizeDAO;
import com.cravefood.cravefood.dao.IngredientDAO;
import com.cravefood.cravefood.dao.SizeDAO;
import com.cravefood.cravefood.dto.DishIngredientDTO;
import com.cravefood.cravefood.dto.DishSizeDTO;
import com.cravefood.cravefood.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DishSizeService {

    private final DishSizeDAO dishSizeDAO;
    private final DishIngredientDAO dishIngredientDAO;
    private final IngredientDAO ingredientDAO;
    private final SizeDAO sizeDAO;

    @Autowired
    public DishSizeService(DishSizeDAO dishSizeDAO, DishIngredientDAO dishIngredientDAO, IngredientDAO ingredientDAO, SizeDAO sizeDAO) {
        this.dishSizeDAO = dishSizeDAO;
        this.dishIngredientDAO = dishIngredientDAO;
        this.ingredientDAO = ingredientDAO;
        this.sizeDAO = sizeDAO;
    }

    public void addDishSize(DishSizeDTO dishSizeDTO, Product product) {
        try {
            Size size = sizeDAO.getById(Size.class, dishSizeDTO.getSizeId());
            DishSize dishSize = new DishSize();
            dishSize.setProduct(product);
            dishSize.setSize(size);
            if (dishSizeDTO.getPrice() > 100000) {
                dishSize.setPrice(dishSizeDTO.getPrice());
            } else {
                dishSize.setPrice(240000.0);
            }
            if (dishSizeDTO.getEnable() < 0) {
                dishSize.setEnable(dishSizeDTO.getEnable());
            } else {
                dishSize.setEnable(1);
            }
            dishSizeDAO.addDishSize(dishSize);
            if (dishSizeDTO.getIngredients() != null && dishSizeDTO.getIngredients().size() != 0) {
                for (DishIngredientDTO item : dishSizeDTO.getIngredients()) {
                    Ingredient ingredient = ingredientDAO.getById(Ingredient.class, item.getIngredientId());
                    DishIngredient dishIngredient = new DishIngredient();
                    dishIngredient.setIngredient(ingredient);
                    dishIngredient.setProduct(product);
                    dishIngredient.setSize(size);
                    if (item.getEnable() >= 0) {
                        dishIngredient.setEnable(item.getEnable());
                    } else {
                        dishIngredient.setEnable(1);
                    }
                    if (item.getQuantity() > 0) {
                        dishIngredient.setQuantity(item.getQuantity());
                    } else {
                        dishIngredient.setQuantity(1000);
                    }
                    dishIngredientDAO.addDishIngredient(dishIngredient);
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeDishSize(DishSize dishSize) {
        dishSizeDAO.removeDishSize(dishSize);
    }

    public void removeDishIngredient(DishIngredient dishIngredient) {
        dishIngredientDAO.deleteDishIngredient(dishIngredient);
    }
}
