package com.cravefood.cravefood.service;

import com.cravefood.cravefood.dao.BannerDAO;
import com.cravefood.cravefood.dto.BannerDTO;
import com.cravefood.cravefood.dto.BannerRequest;
import com.cravefood.cravefood.model.Banner;
import com.cravefood.cravefood.utils.FileUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class BannerService {

    private final BannerDAO bannerDAO;

    @Autowired
    public BannerService(BannerDAO bannerDAO) {
        this.bannerDAO = bannerDAO;
    }

    public List<String> getAllBanner() {
        List<Banner> banners = bannerDAO.getAll(Banner.class);
        List<String> bannerString = new ArrayList<>();
        for (Banner banner : banners) {
            if (banner.getEnable().equals(1)) {
                bannerString.add(banner.getName());
            }
        }
        return bannerString;
    }

    public List<BannerDTO> getAllBannerAdmin() {
        List<Banner> banners = bannerDAO.getAll(Banner.class);
        List<BannerDTO> bannerDTOs = new ArrayList<>();
        for (Banner banner : banners) {
            bannerDTOs.add(BannerDTO.build(banner));
        }
        return bannerDTOs;
    }

    public void deleteBanner(Banner banner) throws IOException {
        URL s = ResourceUtils.getURL("classpath:static/");
        String path = s.getPath() + banner.getName();
        File file = new File(path);
        if(file.delete()) {
            Banner bannerRemove = bannerDAO.getById(Banner.class, banner.getId());
            bannerDAO.delete(bannerRemove);
        }
    }

    public void updateBanner(int id) {
        Banner bannerRemove = bannerDAO.getById(Banner.class, id);
        bannerRemove.setEnable(0);
        bannerDAO.merge(bannerRemove);
    }

    public void addBanner(BannerRequest banner) throws IOException {
        String[] strings = banner.getFile().split(",");
        byte[] data = DatatypeConverter.parseBase64Binary(strings[1]);
        URL s = ResourceUtils.getURL("classpath:static/");
        String path = s.getPath() + "/images/banner/" + banner.getName();
        File file = new File(path);
        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))) {
            outputStream.write(data);
            Banner newBanner = new Banner();
            newBanner.setName("/images/banner/" + banner.getName());
            newBanner.setEnable(1);
            newBanner.setCreatedAt(new Date());
            bannerDAO.merge(newBanner);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
