package com.cravefood.cravefood.service;

import com.cravefood.cravefood.dao.DishSizeDAO;
import com.cravefood.cravefood.dao.OrderDAO;
import com.cravefood.cravefood.dao.OrderItemDAO;
import com.cravefood.cravefood.dao.ProductDAO;
import com.cravefood.cravefood.dto.OrderDTO;
import com.cravefood.cravefood.dto.OrderItemDTO;
import com.cravefood.cravefood.dto.OrderItemQuantityDTO;
import com.cravefood.cravefood.model.DishSize;
import com.cravefood.cravefood.model.Order;
import com.cravefood.cravefood.model.OrderItem;
import com.cravefood.cravefood.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Service
@Transactional
public class OrderService {

    public final OrderDAO orderDAO;
    public final OrderItemDAO orderItemDAO;
    public final ProductDAO productDAO;
    public final DishSizeDAO dishSizeDAO;

    @Autowired
    public OrderService(OrderDAO orderDAO, OrderItemDAO orderItemDAO,ProductDAO productDAO, DishSizeDAO dishSizeDAO) {
        this.orderDAO = orderDAO;
        this.productDAO = productDAO;
        this.orderItemDAO = orderItemDAO;
        this.dishSizeDAO = dishSizeDAO;
    }

    public Order saveOrder(Order order) {
        return orderDAO.saveOrder(order);
    }

    public void saveOrderItem(OrderItem orderItem) {
        orderItemDAO.merge(orderItem);
    }

    public void deleteOrder(int id) {
        Order order = orderDAO.getById(Order.class, id);
        orderDAO.deleteOrder(order);
    }

    public Integer getNextId() {
        return orderDAO.getNextId();
    }

    public List<OrderDTO> getOrders(int page) {
        List<OrderDTO> orderDTOs = new ArrayList<>();
        for (Order order : orderDAO.getInPage(Order.class, page)) {
            OrderDTO orderDTO = OrderDTO.build(order);
            List<OrderItem> orderItems = new ArrayList<>(order.getOrderItems());
            List<OrderItemDTO> orderItemDTOs = new ArrayList<>();
            TreeMap<Integer, List<OrderItem>> map = new TreeMap<>();
            for (OrderItem item : orderItems) {
                if (map.containsKey(item.getProductId())) {
                    map.get(item.getProductId()).add(item);
                } else {
                    List<OrderItem> sameProduct = new ArrayList<>();
                    sameProduct.add(item);
                    map.put(item.getProductId(), sameProduct);
                }
            }
            for (Map.Entry<Integer, List<OrderItem>> entry : map.entrySet()) {
                Product product = productDAO.getById(Product.class, entry.getKey());
                OrderItemDTO orderItemDTO = OrderItemDTO.build(product);
                for (OrderItem item : entry.getValue()) {
                    DishSize dishSize = dishSizeDAO.getDishSize(item.getProductId(), item.getSizeId());
                    orderItemDTO.getSizes().add(OrderItemQuantityDTO.build(dishSize, item));
                }
                orderItemDTOs.add(orderItemDTO);
            }
            orderDTO.setItems(orderItemDTOs);
            orderDTOs.add(orderDTO);
        }
        return orderDTOs;
    }

    public int getRowCount() {
        return orderDAO.getRowCount(Order.class);
    }
}
