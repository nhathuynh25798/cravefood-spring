package com.cravefood.cravefood.service;

import com.cravefood.cravefood.dao.AbstractDAO;
import com.cravefood.cravefood.model.AbstractModel;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(propagation = Propagation.SUPPORTS, rollbackFor = Exception.class)
public class AbstractService<S extends AbstractDAO<T>, T extends AbstractModel> {

    public T getById(Class<T> clazz, Integer id, S dao) {
        return dao.getById(clazz, id);
    }

    public List getAll(Class<T> clazz, S dao) {
        return dao.getAll(clazz);
    }

    public void delete(T model, S dao) {
        dao.delete(model);
    }
}