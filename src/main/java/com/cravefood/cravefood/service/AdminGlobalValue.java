package com.cravefood.cravefood.service;

import com.cravefood.cravefood.dto.MenuDTO;
import com.cravefood.cravefood.model.Ingredient;
import com.cravefood.cravefood.model.Size;

import java.util.HashSet;
import java.util.Set;

public class AdminGlobalValue {

    private Set<MenuDTO> categories;
    private Set<Ingredient> ingredients;
    private Set<Size> sizes;

    public AdminGlobalValue(Set<MenuDTO> categories, Set<Ingredient> ingredients, Set<Size> sizes) {
        this.categories = categories;
        this.ingredients = ingredients;
        this.sizes = sizes;
    }

    public AdminGlobalValue(Set<MenuDTO> categories, Set<Size> sizes) {
        this.categories = categories;
        this.sizes = sizes;
    }

    public Set<MenuDTO> getCategories() {
        return categories;
    }

    public void setCategories(Set<MenuDTO> categories) {
        this.categories = categories;
    }

    public Set<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Set<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public Set<Size> getSizes() {
        return sizes;
    }

    public void setSizes(Set<Size> sizes) {
        this.sizes = sizes;
    }
}
