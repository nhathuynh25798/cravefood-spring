package com.cravefood.cravefood.service;

import com.cravefood.cravefood.dao.WardDAO;
import com.cravefood.cravefood.model.Ward;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class WardService {

    private final WardDAO wardDAO;

    @Autowired
    public WardService(WardDAO wardDAO) {
        this.wardDAO = wardDAO;
    }

    public List getAllWard() {
        return wardDAO.getAll(Ward.class);
    }

    public List getWardsByDistrictId(Integer id) {
        return wardDAO.getWardsByDistrictId(id);
    }
}
