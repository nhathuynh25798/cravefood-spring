package com.cravefood.cravefood.service;

import com.cravefood.cravefood.dao.WishListDAO;
import com.cravefood.cravefood.dto.WishListRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class WishListService {

    private final WishListDAO wishListDAO;

    @Autowired
    public WishListService(WishListDAO wishListDAO) {
        this.wishListDAO = wishListDAO;
    }

    public void saveWishList(WishListRequest wishList) {
        wishListDAO.saveWishList(wishList);
    }

    public void deleteWishList(WishListRequest wishList) {
        wishListDAO.deleteWishList(wishList);
    }
}
