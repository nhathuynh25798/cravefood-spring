package com.cravefood.cravefood.service;

import com.cravefood.cravefood.dao.DistrictDAO;
import com.cravefood.cravefood.model.District;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DistrictService {

    private final DistrictDAO districtDAO;

    @Autowired
    public DistrictService(DistrictDAO districtDAO) {
        this.districtDAO = districtDAO;
    }

    public List getAllDistricts() {
        return districtDAO.getAll(District.class);
    }

    public List getDistrictsByProvinceId(Integer id) {
        return districtDAO.getDistrictsByProvinceId(id);
    }
}
