package com.cravefood.cravefood.service;

import com.cravefood.cravefood.dao.SizeDAO;
import com.cravefood.cravefood.model.Size;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SizeService {

    public final SizeDAO sizeDAO;

    @Autowired
    public SizeService(SizeDAO sizeDAO) {
        this.sizeDAO = sizeDAO;
    }

    public List getAllSize() {
        return sizeDAO.getAll(Size.class);
    }

}
