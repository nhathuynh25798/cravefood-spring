package com.cravefood.cravefood.service;

import com.cravefood.cravefood.dao.AddressDAO;
import com.cravefood.cravefood.model.Address;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AddressService {

    private final AddressDAO addressDAO;

    public AddressService(AddressDAO addressDAO) {
        this.addressDAO = addressDAO;
    }

    public void addAddress(Address address) {
        addressDAO.merge(address);
    }

    public void deleteAddress(Integer id) {
        Address address = addressDAO.getById(Address.class, id);
        addressDAO.delete(address);
    }

    public Address getAddressById(Integer id) {
        return addressDAO.getById(Address.class, id);
    }
}
