package com.cravefood.cravefood.service;

import com.cravefood.cravefood.dao.IngredientDAO;
import com.cravefood.cravefood.model.Ingredient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class IngredientService {

    private final IngredientDAO ingredientDAO;

    @Autowired
    public IngredientService(IngredientDAO ingredientDAO) {
        this.ingredientDAO = ingredientDAO;
    }

    public List getIngredient() {
        return ingredientDAO.getAll(Ingredient.class);
    }

    public void addIngredient(Ingredient ingredient) {
        ingredientDAO.merge(ingredient);
    }

    public void deleteIngredient(int id) {
        Ingredient ingredient = ingredientDAO.getById(Ingredient.class, id);
        ingredientDAO.delete(ingredient);
    }

    public List<Ingredient> getAllIngredient(int page) {
        return ingredientDAO.getInPage(Ingredient.class, page);
    }

    public int getCountRow() {
        return ingredientDAO.getRowCount(Ingredient.class);
    }
}
