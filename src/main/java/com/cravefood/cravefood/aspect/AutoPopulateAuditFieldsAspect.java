package com.cravefood.cravefood.aspect;

import com.cravefood.cravefood.model.AbstractModel;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Aspect
public class AutoPopulateAuditFieldsAspect {

    @SuppressWarnings("rawtypes")
    private void buildMandatoryFields(
            AbstractModel baseEntity) {
        try {
            if (baseEntity.getCreatedAt() == null) {
                baseEntity.setCreatedAt(new Date());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


