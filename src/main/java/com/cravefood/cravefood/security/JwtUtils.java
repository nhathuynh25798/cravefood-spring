package com.cravefood.cravefood.security;

import com.cravefood.cravefood.dto.UserDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.jsonwebtoken.*;
import org.springframework.security.core.Authentication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

@Component
public class JwtUtils {
    private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

    @Value("${cravefood.app.jwtSecret}")
    private String jwtSecret;

    @Value("${cravefood.app.jwtExpirationMs}")
    private int jwtExpirationMs;

    @Value("${cravefood.app.jwtRefreshExpirationMs}")
    private int refreshExpirationDateInMs;

    public String generateJwtToken(Authentication authentication) throws JsonProcessingException {

        UserDTO userPrincipal = (UserDTO) authentication.getPrincipal();

        return Jwts.builder()
                .setSubject(userPrincipal.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .claim("user", userPrincipal)
                .compact();
    }

    public String generateJwtTokenForAdmin(Authentication authentication) throws JsonProcessingException {
        UserDTO userPrincipal = (UserDTO) authentication.getPrincipal();

        if(userPrincipal.getRole().equals("ROLE_ADMIN")) {
          return Jwts.builder()
                    .setSubject(userPrincipal.getUsername())
                    .setIssuedAt(new Date())
                    .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
                    .signWith(SignatureAlgorithm.HS512, jwtSecret)
                    .claim("user", userPrincipal)
                    .compact();
        }
        return null;
    }

    public String getUserNameFromJwtToken(String token) {
        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
    }

    public String doGenerateRefreshToken(Map<String, Object> claims, String subject) {
        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + refreshExpirationDateInMs))
                .signWith(SignatureAlgorithm.HS512, jwtSecret).compact();
    }

    public boolean checkAdminFromToken(String token) {
        boolean isValid = validateJwtToken(token);
        if (isValid) {
            return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().get("user", UserDTO.class).getRole().equals("ROLE_ADMIN");
        } else {
            return false;
        }
    }

    public boolean validateJwtToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            logger.error("Invalid JWT signature: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT token: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            logger.error("JWT token is expired: {}", e.getMessage());
            return true;
        } catch (UnsupportedJwtException e) {
            logger.error("JWT token is unsupported: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            logger.error("JWT claims string is empty: {}", e.getMessage());
        }
        return false;
    }
}
