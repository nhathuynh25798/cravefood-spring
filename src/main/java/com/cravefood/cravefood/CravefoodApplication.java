package com.cravefood.cravefood;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

@SpringBootApplication
public class CravefoodApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(CravefoodApplication.class, args);
    }

    @Override
    public void run(String... args) {
        try {
            File folder = new File("classpath:/static/");
            listFilesForFolder(folder);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void listFilesForFolder(final File folder) {
        for (final File fileEntry : Objects.requireNonNull(folder.listFiles())) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry);
            } else {
                System.out.println(fileEntry.getName());
            }
        }
    }

}
