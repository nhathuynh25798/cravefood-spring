package com.cravefood.cravefood.enums;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}
