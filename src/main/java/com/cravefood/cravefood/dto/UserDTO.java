package com.cravefood.cravefood.dto;

import com.cravefood.cravefood.annotation.Email;
import com.cravefood.cravefood.model.User;
import com.cravefood.cravefood.model.WishList;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.NotBlank;
import java.util.*;

public class UserDTO implements UserDetails {
    private int id;

    @JsonProperty(value = "name")
    private String name;

    @JsonProperty(value = "phone")
    private String phone;

    @Email
    @NotBlank(message = "Email không được bỏ trống")
    @JsonProperty(value = "email")
    private String email;

    @JsonIgnore
    @NotBlank(message = "Password không được bỏ trống")
    @JsonProperty(value = "password")
    private String password;

    @JsonProperty(value = "gender")
    private int gender;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy hh:mm:ss")
    @JsonProperty(value = "birthday")
    private String birthday;

    @JsonIgnore
    private Collection<? extends GrantedAuthority> authorities;

    @JsonProperty(value = "role")
    private String role;

    @JsonProperty(value = "wishLists")
    private List<Integer> wishLists;

    public UserDTO() {
    }

    public static UserDTO build(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        if (user.getName() != null) {
            userDTO.setName(user.getName());
        }
        userDTO.setEmail(user.getEmail());
        userDTO.setPassword(user.getPassword());
        if(user.getPhoneNumber() != null) {
            userDTO.setPhone(user.getPhoneNumber());
        }
        if(user.getGender() != null) {
            userDTO.setGender(user.getGender());
        }
        if(user.getBirthDay() != null) {
            userDTO.setBirthday(user.getBirthDay());
        }
        userDTO.setRole(user.getRole().getName());
        List<Integer> wishLists = new ArrayList<>();
        if(user.getWishLists() != null) {
            for (WishList it : user.getWishLists()) {
                wishLists.add(it.getProduct().getId());
            }
        }
        userDTO.setWishLists(wishLists);
        List<GrantedAuthority> authorities = Collections.singletonList(new SimpleGrantedAuthority(user.getRole().getName()));
        userDTO.setAuthorities(authorities);
        return userDTO;
    }

    public UserDTO(int id, String name, String phone, @NotBlank(message = "Email không được bỏ trống") String email, @NotBlank(message = "Password không được bỏ trống") String password, int gender, String birthday, Collection<? extends GrantedAuthority> authorities, String role, List<Integer> wishLists) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.password = password;
        this.gender = gender;
        this.birthday = birthday;
        this.authorities = authorities;
        this.role = role;
        this.wishLists = wishLists;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<Integer> getWishLists() {
        return wishLists;
    }

    public void setWishLists(List<Integer> wishLists) {
        this.wishLists = wishLists;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        UserDTO user = (UserDTO) o;
        return Objects.equals(id, user.id);
    }
}
