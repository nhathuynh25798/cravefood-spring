package com.cravefood.cravefood.dto;

import com.cravefood.cravefood.model.Product;

import java.util.ArrayList;
import java.util.List;

public class OrderItemDTO {
    private String name;
    private double discount;
    private String image;
    private String slug;
    private int enable;
    private List<OrderItemQuantityDTO> sizes = new ArrayList<>();
    
    public OrderItemDTO() {
    }

    public static OrderItemDTO build(Product product) {
        OrderItemDTO orderItemDTO = new OrderItemDTO();
        orderItemDTO.setName(product.getName());
        orderItemDTO.setSlug(product.getSlug());
        orderItemDTO.setEnable(product.getEnable());
        orderItemDTO.setImage(product.getImages().iterator().next().getContent());
        if(product.getDiscount() != null) {
            orderItemDTO.setDiscount(product.getDiscount());
        }
        return orderItemDTO;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }

    public List<OrderItemQuantityDTO> getSizes() {
        return sizes;
    }

    public void setSizes(List<OrderItemQuantityDTO> sizes) {
        this.sizes = sizes;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
