package com.cravefood.cravefood.dto;

import com.cravefood.cravefood.model.DishSize;
import com.cravefood.cravefood.model.OrderItem;

public class OrderItemQuantityDTO {

    private int orderItemId;
    private int productId;
    private String sizeName;
    private int sizeId;
    private String sizeImage;
    private double price;
    private int enable;
    private int quantity;

    public OrderItemQuantityDTO() {
    }
    
    public static OrderItemQuantityDTO build(DishSize dishSize, OrderItem orderItem) {
        OrderItemQuantityDTO orderItemQuantityDTO = new OrderItemQuantityDTO();
        orderItemQuantityDTO.setOrderItemId(orderItem.getId());
        orderItemQuantityDTO.setProductId(dishSize.getProduct().getId());
        orderItemQuantityDTO.setQuantity(orderItem.getQuantity());
        orderItemQuantityDTO.setSizeId(dishSize.getSize().getId());
        orderItemQuantityDTO.setSizeImage(dishSize.getSize().getImage());
        orderItemQuantityDTO.setSizeName(dishSize.getSize().getName());
        orderItemQuantityDTO.setEnable(dishSize.getEnable());
        orderItemQuantityDTO.setPrice(dishSize.getPrice());
        return orderItemQuantityDTO;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public int getSizeId() {
        return sizeId;
    }

    public void setSizeId(int sizeId) {
        this.sizeId = sizeId;
    }

    public String getSizeImage() {
        return sizeImage;
    }

    public void setSizeImage(String sizeImage) {
        this.sizeImage = sizeImage;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(int orderItemId) {
        this.orderItemId = orderItemId;
    }
}
