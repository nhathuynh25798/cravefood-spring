package com.cravefood.cravefood.dto;

import com.cravefood.cravefood.model.DishSize;
import com.cravefood.cravefood.model.Product;
import com.cravefood.cravefood.model.ProductImage;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ProductsItemDTO {

    private int id;
    private String name;
    private double discount;
    private List<ProductImageDTO> images;
    private Set<DishSizeNotIngredientsDTO> sizes;
    private String slug;
    private int enable;

    public ProductsItemDTO() {
    }

    public static ProductsItemDTO build(Product product) {
        ProductsItemDTO productsItemDTO = new ProductsItemDTO();
        productsItemDTO.setId(product.getId());
        productsItemDTO.setName(product.getName());
        List<ProductImageDTO> images = new ArrayList<>();
        if (product.getImages() != null) {
            for (ProductImage productImage: product.getImages()) {
                images.add(ProductImageDTO.build(productImage));
            }
        }
        productsItemDTO.setImages(images);
        productsItemDTO.setSlug(product.getSlug());
        if(product.getDiscount() != null) {
            productsItemDTO.setDiscount(product.getDiscount());
        }
        Set<DishSizeNotIngredientsDTO> sizes = new HashSet<>();
        for (DishSize dishSize: product.getSizes()) {
            sizes.add(DishSizeNotIngredientsDTO.build(dishSize));
        }
        productsItemDTO.setSizes(sizes);
        productsItemDTO.setEnable(product.getEnable());
        return productsItemDTO;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public List<ProductImageDTO> getImages() {
        return images;
    }

    public void setImages(List<ProductImageDTO> images) {
        this.images = images;
    }

    public Set<DishSizeNotIngredientsDTO> getSizes() {
        return sizes;
    }

    public void setSizes(Set<DishSizeNotIngredientsDTO> sizes) {
        this.sizes = sizes;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}
