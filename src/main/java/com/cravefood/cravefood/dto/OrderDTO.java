package com.cravefood.cravefood.dto;

import com.cravefood.cravefood.model.Order;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

public class OrderDTO {

    @JsonProperty(value = "id")
    private int id;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern= "dd/MM/yyyy hh:mm:ss")
    @JsonProperty(value = "createAt")
    private Date createdAt;

    @JsonProperty(value = "enable")
    private int enable;

    @JsonProperty(value = "deliveryDate")
    private String deliveryDate;

    @JsonProperty(value = "name")
    private String name;

    @JsonProperty(value = "detail")
    private String addressDetail;

    @JsonProperty(value = "ward")
    private String ward;

    @JsonProperty(value = "district")
    private String district;

    @JsonProperty(value = "province")
    private String province;

    @JsonProperty(value = "phone")
    private String addressPhoneContact;

    @JsonProperty(value = "note")
    private String note;

    @JsonProperty(value = "addressNote")
    private String addressNote;

    @JsonProperty(value = "codeString")
    private String codeString;

    @JsonProperty(value = "status")
    private Integer status;

    @JsonProperty(value = "cartTotalDiscountPrice")
    private Double totalPrice;

    @JsonProperty(value = "cartTotalOriginalPrice")
    private Double totalOriginPrice;

    @JsonProperty(value = "items")
    private List<OrderItemDTO> items;

    public OrderDTO() {
    }

    public static OrderDTO build(Order order) {
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setId(order.getId());
        orderDTO.setName(order.getName());
        orderDTO.setCreatedAt(order.getCreatedAt());
        orderDTO.setEnable(order.getEnable());
        orderDTO.setDeliveryDate(order.getDeliveryDate());
        orderDTO.setAddressDetail(order.getAddressDetail());
        orderDTO.setWard(order.getWard());
        orderDTO.setDistrict(order.getDistrict());
        orderDTO.setProvince(order.getProvince());
        orderDTO.setAddressPhoneContact(order.getAddressPhoneContact());
        orderDTO.setAddressNote(order.getAddressNote());
        orderDTO.setCodeString(order.getCodeString());
        orderDTO.setStatus(order.getStatus());
        orderDTO.setTotalPrice(order.getTotalPrice());
        orderDTO.setTotalOriginPrice(order.getTotalOriginPrice());
        return orderDTO;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public String getAddressPhoneContact() {
        return addressPhoneContact;
    }

    public void setAddressPhoneContact(String addressPhoneContact) {
        this.addressPhoneContact = addressPhoneContact;
    }

    public String getAddressNote() {
        return addressNote;
    }

    public void setAddressNote(String addressNote) {
        this.addressNote = addressNote;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Double getTotalOriginPrice() {
        return totalOriginPrice;
    }

    public void setTotalOriginPrice(Double totalOriginPrice) {
        this.totalOriginPrice = totalOriginPrice;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<OrderItemDTO> getItems() {
        return items;
    }

    public void setItems(List<OrderItemDTO> items) {
        this.items = items;
    }

    public String getCodeString() {
        return codeString;
    }

    public void setCodeString(String codeString) {
        this.codeString = codeString;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
