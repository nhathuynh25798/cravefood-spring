package com.cravefood.cravefood.dto;

import com.cravefood.cravefood.annotation.Email;
import com.cravefood.cravefood.annotation.Phone;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;

public class RegisterDTO {

    @Email
    @NotBlank(message = "Email không được bỏ trống")
    @JsonProperty("email")
    private String email;

    @Phone
    @NotBlank(message = "Số điện thoại không được bỏ trống")
    @JsonProperty("phone")
    private String phone;

    @NotBlank(message = "Password không được bỏ trống")
    @JsonProperty("password")
    private String password;

    public RegisterDTO() {}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
