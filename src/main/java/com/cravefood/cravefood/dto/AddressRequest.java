package com.cravefood.cravefood.dto;

import com.cravefood.cravefood.model.Address;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class AddressRequest {

    @JsonProperty(value = "addressList")
    private List<Address> addresses;

    public AddressRequest() {
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }
}
