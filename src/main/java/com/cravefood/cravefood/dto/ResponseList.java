package com.cravefood.cravefood.dto;

import java.util.List;

public class ResponseList<T> {

    private List<T> list;
    private int total;

    public ResponseList(List<T> list, int total) {
        this.list = list;
        this.total = total;
    }

    public ResponseList() {
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
