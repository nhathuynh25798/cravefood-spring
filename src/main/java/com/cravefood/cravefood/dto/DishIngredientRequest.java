package com.cravefood.cravefood.dto;

public class DishIngredientRequest {
    public int productId;
    public int sizeId;
    public int ingredientId;

    public DishIngredientRequest() {
    }

    public int getProductId() {
        return productId;
    }

    public int getSizeId() {
        return sizeId;
    }

    public int getIngredientId() {
        return ingredientId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public void setSizeId(int sizeId) {
        this.sizeId = sizeId;
    }

    public void setIngredientId(int ingredientId) {
        this.ingredientId = ingredientId;
    }
}
