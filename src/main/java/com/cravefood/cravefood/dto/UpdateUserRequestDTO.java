package com.cravefood.cravefood.dto;

import com.cravefood.cravefood.annotation.Phone;

import javax.validation.constraints.NotBlank;

public class UpdateUserRequestDTO {

    private int id;
    private String name;
    @Phone
    @NotBlank(message = "Số điện thoại không được bỏ trống")
    private String phone;
    private int gender;
    private String birthday;

    public UpdateUserRequestDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
}
