package com.cravefood.cravefood.dto;

import com.cravefood.cravefood.model.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.*;

public class ProductDTO {

    @JsonProperty("id")
    private int id;

    @JsonProperty("name")
    private String name;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern= "dd/MM/yyyy hh:mm:ss")
    @JsonProperty("createAt")
    private Date createdAt;

    @JsonProperty("createdBy")
    private int createdBy;

    @JsonProperty("enable")
    private int enable;

    @JsonProperty("images")
    private List<ProductImageDTO> images;

    @JsonProperty("description")
    private String description;

    @JsonProperty("discount")
    private double discount;

    @JsonProperty("slug")
    private String slug;

    @JsonProperty("sizes")
    private Set<DishSizeDTO> sizes;

    @JsonProperty("categories")
    private List<MenuDTO> categories;

    @JsonProperty("files")
    private List<StringBase64File> stringBase64Files = new ArrayList<>();

    public ProductDTO() {}

    public static ProductDTO build(Product product) {
        ProductDTO productDTO = new ProductDTO();
        if(product.getId() != null) {
            productDTO.setId(product.getId());
        }
        if(product.getName() != null) {
            productDTO.setName(product.getName());
        }
        productDTO.setCreatedAt(product.getCreatedAt());
        productDTO.setCreatedBy(product.getCreateBy());
        productDTO.setEnable(product.getEnable());
        List<ProductImageDTO> images = new ArrayList<>();
        if (product.getImages() != null) {
            for (ProductImage productImage: product.getImages()) {
                images.add(ProductImageDTO.build(productImage));
            }
        }
        productDTO.setImages(images);
        if(product.getDescription() != null) {
            productDTO.setDescription(product.getDescription());
        }
        productDTO.setSlug(product.getSlug());
        if (product.getDiscount() != null) {
            productDTO.setDiscount(product.getDiscount());
        }
        List<MenuDTO> categories = new ArrayList<>();
        if(product.getCategories() != null) {
            for(Category category : product.getCategories()) {
                categories.add(MenuDTO.build(category));
            }
        }
        productDTO.setCategories(categories);
        Set<DishSizeDTO> sizes = new HashSet<>();
        if(product.getSizes() != null) {
            for (DishSize dishSize: product.getSizes()) {
                sizes.add(DishSizeDTO.build(dishSize));
            }
        }
        productDTO.setSizes(sizes);
        return productDTO;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }

    public List<ProductImageDTO> getImages() {
        return images;
    }

    public void setImages(List<ProductImageDTO> images) {
        this.images = images;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Set<DishSizeDTO> getSizes() {
        return sizes;
    }

    public void setSizes(Set<DishSizeDTO> sizes) {
        this.sizes = sizes;
    }

    public List<MenuDTO> getCategories() {
        return categories;
    }

    public void setCategories(List<MenuDTO> categories) {
        this.categories = categories;
    }

    public List<StringBase64File> getStringBase64Files() {
        return stringBase64Files;
    }

    public void setStringBase64Files(List<StringBase64File> stringBase64Files) {
        this.stringBase64Files = stringBase64Files;
    }
}

