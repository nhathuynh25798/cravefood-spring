package com.cravefood.cravefood.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class OrderRequest {

    private int userId, total, enable, status;
    private String name, detail, ward, district, province, phone, note, addressNote, deliveryDate, codeString;
    @JsonProperty("items")
    private List<OrderItemRequest> orderItemRequest;
    private double cartTotalOriginalPrice, cartTotalDiscountPrice;

//    public OrderRequest(int userId, int total, int enable, int status,
//                        String name, String detail, String ward, String district,
//                        String province, String phone, String note, String addressNote,
//                        String deliveryDate, String codeString, List<OrderItemRequest> orderItemRequest,
//                        double cartTotalOriginalPrice, double cartTotalDiscountPrice) {
//        this.userId = userId;
//        this.total = total;
//        this.enable = enable;
//        this.status = status;
//        this.name = name;
//        this.detail = detail;
//        this.ward = ward;
//        this.district = district;
//        this.province = province;
//        this.phone = phone;
//        this.note = note;
//        this.addressNote = addressNote;
//        this.deliveryDate = deliveryDate;
//        this.codeString = codeString;
//        this.orderItemRequest = orderItemRequest;
//        this.cartTotalOriginalPrice = cartTotalOriginalPrice;
//        this.cartTotalDiscountPrice = cartTotalDiscountPrice;
//    }


    public OrderRequest() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getAddressNote() {
        return addressNote;
    }

    public void setAddressNote(String addressNote) {
        this.addressNote = addressNote;
    }

    public List<OrderItemRequest> getOrderItemRequest() {
        return orderItemRequest;
    }

    public void setOrderItemRequest(List<OrderItemRequest> orderItemRequest) {
        this.orderItemRequest = orderItemRequest;
    }

    public double getCartTotalOriginalPrice() {
        return cartTotalOriginalPrice;
    }

    public void setCartTotalOriginalPrice(double cartTotalOriginalPrice) {
        this.cartTotalOriginalPrice = cartTotalOriginalPrice;
    }

    public double getCartTotalDiscountPrice() {
        return cartTotalDiscountPrice;
    }

    public void setCartTotalDiscountPrice(double cartTotalDiscountPrice) {
        this.cartTotalDiscountPrice = cartTotalDiscountPrice;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getCodeString() {
        return codeString;
    }

    public void setCodeString(String codeString) {
        this.codeString = codeString;
    }
}
