package com.cravefood.cravefood.dto;

import com.cravefood.cravefood.model.WishList;
import com.fasterxml.jackson.annotation.JsonProperty;

public class WishListDTO {

    @JsonProperty(value = "product")
    private ProductsItemDTO product;

    public WishListDTO() {
    }

    public static WishListDTO build(WishList wishList) {
        WishListDTO wishListDTO = new WishListDTO();
        wishListDTO.setProduct(ProductsItemDTO.build(wishList.getProduct()));
        return wishListDTO;
    }

    public ProductsItemDTO getProduct() {
        return product;
    }

    public void setProduct(ProductsItemDTO product) {
        this.product = product;
    }
}
