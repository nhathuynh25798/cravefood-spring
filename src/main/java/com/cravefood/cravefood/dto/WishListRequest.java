package com.cravefood.cravefood.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WishListRequest {

    @JsonProperty( value = "productId")
    private int productId;

    @JsonProperty(value = "userId")
    private int userId;

    public WishListRequest() {
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
