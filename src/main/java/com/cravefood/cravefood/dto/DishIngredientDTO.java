package com.cravefood.cravefood.dto;

import com.cravefood.cravefood.model.DishIngredient;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DishIngredientDTO {

    @JsonProperty("ingredient")
    private String ingredient;

    @JsonProperty("ingredientId")
    private int ingredientId;

    @JsonProperty("unit")
    private String unit;

    @JsonProperty("quantity")
    private int quantity;

    @JsonProperty("enable")
    private int enable;

    public DishIngredientDTO() {
    }

    public static DishIngredientDTO build(DishIngredient dishIngredient) {
        DishIngredientDTO dishIngredientDTO = new DishIngredientDTO();
        dishIngredientDTO.setIngredient(dishIngredient.getIngredient().getName());
        dishIngredientDTO.setIngredientId(dishIngredient.getIngredient().getId());
        dishIngredientDTO.setUnit(dishIngredient.getIngredient().getUnit());
        dishIngredientDTO.setQuantity(dishIngredient.getQuantity());
        dishIngredientDTO.setEnable(dishIngredient.getEnable());
        return dishIngredientDTO;
    }

    public String getIngredient() {
        return ingredient;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    public int getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(int ingredientId) {
        this.ingredientId = ingredientId;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }
}
