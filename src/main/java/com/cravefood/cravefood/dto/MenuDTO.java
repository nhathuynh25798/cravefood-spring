package com.cravefood.cravefood.dto;

import com.cravefood.cravefood.model.Category;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MenuDTO {

    @JsonProperty("id")
    private int id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("showHome")
    private int showHome;

    @JsonProperty("showMenu")
    private int showMenu;

    @JsonProperty("slug")
    private String slug;

    public MenuDTO() {
    }

    public static MenuDTO build(Category category) {
        MenuDTO menu = new MenuDTO();
        menu.setId(category.getId());
        menu.setName(category.getName());
        menu.setSlug(category.getSlug());
        menu.setShowHome(category.getShowHome());
        menu.setShowMenu(category.getShowMenu());
        return menu;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public int getShowHome() {
        return showHome;
    }

    public void setShowHome(int showHome) {
        this.showHome = showHome;
    }

    public int getShowMenu() {
        return showMenu;
    }

    public void setShowMenu(int showMenu) {
        this.showMenu = showMenu;
    }
}
