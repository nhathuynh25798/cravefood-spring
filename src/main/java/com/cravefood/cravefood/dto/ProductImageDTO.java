package com.cravefood.cravefood.dto;

import com.cravefood.cravefood.model.ProductImage;

public class ProductImageDTO {

    private int id;
    private String content;

    public ProductImageDTO() {
    }

    public static ProductImageDTO build(ProductImage productImage) {
        ProductImageDTO productImageDTO = new ProductImageDTO();
        productImageDTO.setId(productImage.getId());
        productImageDTO.setContent(productImage.getContent());
        return productImageDTO;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
