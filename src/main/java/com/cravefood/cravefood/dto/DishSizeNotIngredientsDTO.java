package com.cravefood.cravefood.dto;

import com.cravefood.cravefood.model.DishSize;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DishSizeNotIngredientsDTO {

    @JsonProperty("productId")
    private int productId;

    @JsonProperty("sizeName")
    private String sizeName;

    @JsonProperty("sizeId")
    private int sizeId;

    @JsonProperty("sizeImage")
    private String sizeImage;

    @JsonProperty("price")
    private double price;

    @JsonProperty("enable")
    private int enable;

    public DishSizeNotIngredientsDTO() {
    }

    public static DishSizeNotIngredientsDTO build(DishSize dishSize) {
        DishSizeNotIngredientsDTO dishSizeNotIngredientsDTO = new DishSizeNotIngredientsDTO();
        dishSizeNotIngredientsDTO.setProductId(dishSize.getProduct().getId());
        dishSizeNotIngredientsDTO.setSizeId(dishSize.getSize().getId());
        dishSizeNotIngredientsDTO.setSizeImage(dishSize.getSize().getImage());
        dishSizeNotIngredientsDTO.setSizeName(dishSize.getSize().getName());
        dishSizeNotIngredientsDTO.setEnable(dishSize.getEnable());
        dishSizeNotIngredientsDTO.setPrice(dishSize.getPrice());
        return dishSizeNotIngredientsDTO;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public int getSizeId() {
        return sizeId;
    }

    public void setSizeId(int sizeId) {
        this.sizeId = sizeId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getSizeImage() {
        return sizeImage;
    }

    public void setSizeImage(String sizeImage) {
        this.sizeImage = sizeImage;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }
}
