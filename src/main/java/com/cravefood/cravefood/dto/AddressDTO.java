package com.cravefood.cravefood.dto;

import com.cravefood.cravefood.annotation.Phone;
import com.cravefood.cravefood.model.Address;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import java.util.Date;

public class AddressDTO {

    @JsonProperty(value = "id")
    private int id;

    @JsonProperty(value = "name")
    private String name;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern= "dd/MM/yyyy hh:mm:ss")
    @JsonProperty("createAt")
    private Date createdAt;

    @JsonProperty(value = "detail")
    private String detail;

    @JsonProperty(value = "ward")
    private String ward;

    @JsonProperty(value = "district")
    private String district;

    @JsonProperty(value = "province")
    private String province;

    @Phone
    @NotBlank(message = "Số điện thoại không được bỏ trống")
    @JsonProperty(value = "phone")
    private String phoneContact;

    @JsonProperty(value = "defaultAddressValue")
    private int defaultValue;

    @JsonProperty(value = "userId")
    private int createBy;

    @JsonProperty(value = "note")
    private String note;

    @JsonProperty(value = "enable")
    private int enable;

    public AddressDTO() {
    }

    public static AddressDTO build(Address address) {
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setId(address.getId());
        addressDTO.setCreateBy(address.getCreateBy());
        addressDTO.setName(address.getName());
        addressDTO.setCreatedAt(address.getCreatedAt());
        addressDTO.setDetail(address.getDetail());
        addressDTO.setWard(address.getWard());
        addressDTO.setDistrict(address.getDistrict());
        addressDTO.setProvince(address.getProvince());
        addressDTO.setPhoneContact(address.getPhoneContact());
        if(address.getDefaultAddress() == null) {
            addressDTO.setDefaultValue(0);
        } else {
            addressDTO.setDefaultValue(address.getDefaultAddress());
        }
        if(address.getNote() != null) {
            addressDTO.setNote(address.getNote());
        }
        addressDTO.setEnable(address.getEnable());
        return addressDTO;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPhoneContact() {
        return phoneContact;
    }

    public void setPhoneContact(String phoneContact) {
        this.phoneContact = phoneContact;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(int defaultValue) {
        this.defaultValue = defaultValue;
    }

    public int getCreateBy() {
        return createBy;
    }

    public void setCreateBy(int createBy) {
        this.createBy = createBy;
    }
}
