package com.cravefood.cravefood.dto;

public class StringBase64File {

    private String name;
    private String file;

    public StringBase64File() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
