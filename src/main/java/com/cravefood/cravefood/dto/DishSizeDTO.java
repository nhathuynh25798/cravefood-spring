package com.cravefood.cravefood.dto;

import com.cravefood.cravefood.model.DishIngredient;
import com.cravefood.cravefood.model.DishSize;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashSet;
import java.util.Set;

public class DishSizeDTO {

    @JsonProperty("productId")
    private int productId;

    @JsonProperty("sizeName")
    private String sizeName;

    @JsonProperty("sizeId")
    private int sizeId;

    @JsonProperty("sizeImage")
    private String sizeImage;

    @JsonProperty("price")
    private double price;

    @JsonProperty("enable")
    private int enable;

    @JsonProperty("ingredients")
    private Set<DishIngredientDTO> ingredients;

    public DishSizeDTO() {
    }

    public static DishSizeDTO build(DishSize dishSize) {
        DishSizeDTO dishSizeDTO = new DishSizeDTO();
        dishSizeDTO.setProductId(dishSize.getProduct().getId());
        dishSizeDTO.setSizeId(dishSize.getSize().getId());
        dishSizeDTO.setSizeImage(dishSize.getSize().getImage());
        dishSizeDTO.setSizeName(dishSize.getSize().getName());
        dishSizeDTO.setPrice(dishSize.getPrice());
        dishSizeDTO.setEnable(dishSize.getEnable());
        Set<DishIngredientDTO> ingredients = new HashSet<>();
        for (DishIngredient dishIngredient : dishSize.getIngredients()) {
            ingredients.add(DishIngredientDTO.build(dishIngredient));
        }
        dishSizeDTO.setIngredients(ingredients);
        return dishSizeDTO;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getSizeId() {
        return sizeId;
    }

    public void setSizeId(int sizeId) {
        this.sizeId = sizeId;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Set<DishIngredientDTO> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Set<DishIngredientDTO> ingredients) {
        this.ingredients = ingredients;
    }

    public String getSizeImage() {
        return sizeImage;
    }

    public void setSizeImage(String sizeImage) {
        this.sizeImage = sizeImage;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }
}
