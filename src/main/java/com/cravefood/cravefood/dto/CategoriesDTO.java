package com.cravefood.cravefood.dto;

import com.cravefood.cravefood.model.Category;
import com.cravefood.cravefood.model.Product;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class CategoriesDTO {
    @JsonProperty("id")
    private int id;

    @JsonProperty("name")
    private String name;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern= "dd/MM/yyyy hh:mm:ss")
    @JsonProperty("createAt")
    private Date createAt;

    @JsonProperty("createBy")
    private int createBy;

    @JsonProperty("enable")
    private int enable;

    @JsonProperty("showHome")
    private int showHome;

    @JsonProperty("showMenu")
    private int showMenu;

    @JsonProperty("slug")
    private String slug;

    @JsonProperty("products")
    private Set<ProductsItemDTO> products;

    public CategoriesDTO() {}

    public static CategoriesDTO build(Category category) {
        CategoriesDTO categoriesDTO = new CategoriesDTO();
        if(category.getId() != null) {
            categoriesDTO.setId(category.getId());
        }
        if(category.getName() != null) {
            categoriesDTO.setName(category.getName());
        }
        categoriesDTO.setCreateAt(category.getCreatedAt());
        if(category.getCreateBy() != null) {
            categoriesDTO.setCreateBy(category.getCreateBy());
        }
        if(category.getEnable() != null) {
            categoriesDTO.setEnable(category.getEnable());
        }
        if(category.getShowHome() != null) {
            categoriesDTO.setShowHome(category.getShowHome());
        }
        if(category.getShowMenu() != null) {
            categoriesDTO.setShowMenu(category.getShowMenu());
        }
        if(category.getSlug() !=null) {
            categoriesDTO.setSlug(category.getSlug());
        }
        Set<ProductsItemDTO> products = new HashSet<>();
        if(category.getProducts() != null && !category.getProducts().isEmpty()) {
            for (Product product: category.getProducts()) {
                products.add(ProductsItemDTO.build(product));
            }
        }
        categoriesDTO.setProducts(products);
        return categoriesDTO;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public int getCreateBy() {
        return createBy;
    }

    public void setCreateBy(int createBy) {
        this.createBy = createBy;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Set<ProductsItemDTO> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductsItemDTO> products) {
        this.products = products;
    }

    public int getShowHome() {
        return showHome;
    }

    public void setShowHome(int showHome) {
        this.showHome = showHome;
    }

    public int getShowMenu() {
        return showMenu;
    }

    public void setShowMenu(int showMenu) {
        this.showMenu = showMenu;
    }
}
