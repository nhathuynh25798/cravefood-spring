package com.cravefood.cravefood.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

public class GlobalValue {

    @JsonProperty("categories")
    private Set<MenuDTO> showMenu;

    @JsonProperty("showHomePage")
    private Set<CategoriesDTO> showHomePage;

    @JsonProperty("banners")
    private Set<String> banners;

    public GlobalValue(Set<MenuDTO> showMenu, Set<CategoriesDTO> showHomePage, Set<String> banners) {
        this.showMenu = showMenu;
        this.showHomePage = showHomePage;
        this.banners = banners;
    }

    public Set<MenuDTO> getShowMenu() {
        return showMenu;
    }

    public void setShowMenu(Set<MenuDTO> showMenu) {
        this.showMenu = showMenu;
    }

    public Set<CategoriesDTO> getShowHomePage() {
        return showHomePage;
    }

    public void setShowHomePage(Set<CategoriesDTO> showHomePage) {
        this.showHomePage = showHomePage;
    }

    public Set<String> getBanners() {
        return banners;
    }

    public void setBanners(Set<String> banners) {
        this.banners = banners;
    }
}
