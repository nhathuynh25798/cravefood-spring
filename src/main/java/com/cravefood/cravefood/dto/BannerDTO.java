package com.cravefood.cravefood.dto;

import com.cravefood.cravefood.model.Banner;

public class BannerDTO {
    private int id;
    private String name;
    private int enable;

    public BannerDTO() {
    }

    public static BannerDTO build(Banner banner) {
        BannerDTO bannerDTO = new BannerDTO();
        bannerDTO.setId(banner.getId());
        bannerDTO.setName(banner.getName());
        bannerDTO.setEnable(banner.getEnable());
        return bannerDTO;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }
}
