package com.cravefood.cravefood.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@IdClass(WishList.WishListId.class)
@Table(name = "wishlist")
@JsonSerialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WishList implements Serializable {

    @Id
    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "user_id", nullable = false)
    private User createBy;

    @Id
    @ManyToOne
    @JoinColumn(name = "product_id", nullable = true)
    private Product product;

    @Column(name = "enable")
    private int enable;

    public User getCreateBy() {
        return createBy;
    }

    public void setCreateBy(User createBy) {
        this.createBy = createBy;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }

    public static class WishListId implements Serializable {

        private Product product;

        private User createBy;

        public WishListId() {
            super();
        }

        public WishListId(Product product, User createBy) {
            this.product = product;
            this.createBy = createBy;
        }

        public Product getProduct() {
            return product;
        }

        public void setProduct(Product product) {
            this.product = product;
        }

        public User getCreateBy() {
            return createBy;
        }

        public void setCreateBy(User createBy) {
            this.createBy = createBy;
        }
    }
}
