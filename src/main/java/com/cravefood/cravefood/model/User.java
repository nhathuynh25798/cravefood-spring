package com.cravefood.cravefood.model;

import com.cravefood.cravefood.annotation.Email;
import com.cravefood.cravefood.annotation.Phone;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "`user`",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "email")
        })
public class User extends AbstractModel implements Serializable {

    public User(@NotBlank(message = "Email không được bỏ trống") String email, @NotBlank(message = "Password không được bỏ trống") String password) {
        this.email = email;
        this.password = password;
    }

    @Email
    @NotBlank(message = "Email không được bỏ trống")
    @Column(name = "email")
    private String email;

    @JsonIgnore
    @NotBlank(message = "Password không được bỏ trống")
    @Column(name = "password")
    private String password;

    @Phone
    @NotBlank(message = "Số điện thoại không được bỏ trống")
    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "gender")
    private Integer gender;

    @Column(name = "birthday")
    private String birthDay;

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "role_id", nullable = false)
    private Role role;

//    @Temporal(TemporalType.TIMESTAMP)
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern= "yyyy-MM-dd hh:mm")
//    @Column(name = "last_login")
//    private Date lastLogin;
//
//    @Column(name = "token")
//    private String accessToken;

    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "createBy", cascade = CascadeType.ALL)
    private Set<Address> addresses = new HashSet<>();

    @Fetch(FetchMode.SELECT)
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "createBy", cascade = CascadeType.ALL)
    private Set<Order> orders = new HashSet<>();

    @Fetch(FetchMode.SELECT)
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "createBy", cascade = CascadeType.ALL)
    private Set<WishList> wishLists = new HashSet<>();

    @Transient
    private Boolean changePassword;

    public User() {
        super();
    }

    public User(Set<Order> orders) {
        this.orders = orders;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }

    public Set<WishList> getWishLists() {
        return wishLists;
    }

    public void setWishLists(Set<WishList> wishLists) {
        this.wishLists = wishLists;
    }

    //    public Date getLastLogin() {
//        return lastLogin;
//    }
//
//    public void setLastLogin(Date lastLogin) {
//        this.lastLogin = lastLogin;
//    }
//
//    public String getAccessToken() {
//        return accessToken;
//    }
//
//    public void setAccessToken(String accessToken) {
//        this.accessToken = accessToken;
//    }

    public Boolean getChangePassword() {
        return changePassword;
    }

    public void setChangePassword(Boolean changePassword) {
        this.changePassword = changePassword;
    }
}
