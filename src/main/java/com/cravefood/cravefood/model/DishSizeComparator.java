package com.cravefood.cravefood.model;

import java.util.Comparator;

public class DishSizeComparator implements Comparator<DishSize> {

    @Override
    public int compare(DishSize dishSize, DishSize t1) {
        return dishSize.getPrice().compareTo(t1.getPrice());
    }
}
