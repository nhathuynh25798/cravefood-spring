package com.cravefood.cravefood.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "`order`")
public class Order extends AbstractModel {

    @Column(name = "delivery_date")
    private String deliveryDate;

    @Column(name = "detail")
    private String addressDetail;

    @Column(name = "province")
    private String province;

    @Column(name = "district")
    private String district;

    @Column(name = "ward")
    private String ward;

    @Column(name = "phone")
    private String addressPhoneContact;

    @JsonProperty("addressNote")
    @Column(name = "address_note")
    private String addressNote;

    @Column(name = "status")
    private Integer status;

    @JsonProperty("cartTotalDiscountPrice")
    @Column(name = "total_price")
    private Double totalPrice;

    @JsonProperty("cartTotalOriginalPrice")
    @Column(name = "total_original_price")
    private Double totalOriginPrice;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "order", cascade = CascadeType.ALL)
    private Set<OrderItem> orderItems = new HashSet<>();

    @Column(name = "note")
    private String note;

    @Column(name = "codestring")
    private String codeString;

    @Column(name = "total")
    private Integer total;

    public Order() { super(); }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Double getTotalOriginPrice() {
        return totalOriginPrice;
    }

    public void setTotalOriginPrice(Double totalOriginPrice) {
        this.totalOriginPrice = totalOriginPrice;
    }

    public Set<OrderItem> getOrderDetails() {
        return orderItems;
    }

    public void setOrderDetails(Set<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public Set<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(Set<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCodeString() {
        return codeString;
    }

    public void setCodeString(String codeString) {
        this.codeString = codeString;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public String getAddressPhoneContact() {
        return addressPhoneContact;
    }

    public void setAddressPhoneContact(String addressPhoneContact) {
        this.addressPhoneContact = addressPhoneContact;
    }

    public String getAddressNote() {
        return addressNote;
    }

    public void setAddressNote(String addressNote) {
        this.addressNote = addressNote;
    }
}
