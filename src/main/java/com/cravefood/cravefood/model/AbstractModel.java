package com.cravefood.cravefood.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@MappedSuperclass
@JsonSerialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class AbstractModel implements Serializable {

    @JsonProperty(value = "id")
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @JsonProperty(value = "name")
    @Column(name = "`name`")
    private String name;

    @JsonProperty(value = "enable")
    @Column(name = "`enable`")
    private Integer enable;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern= "dd/MM/yyyy hh:mm:ss")
    @Column(name = "create_at")
    private Date createdAt;

    @JsonProperty(value = "userId")
    @Column(name = "create_by")
    private Integer createBy;

    public AbstractModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AbstractModel other = (AbstractModel) obj;
        return id.equals(other.id);
    }

    public String abstractToString() {
        StringBuilder builder = new StringBuilder();
        builder.append("\tid: ").append(this.id);
        if (!name.equals("")) {
            builder.append(",\n\tname: ").append(this.name);
        }
        builder.append(",\n\tenable: ").append(this.enable == 0 ? "true" : "false");
        builder.append(",\n\tcreateBy: ").append(this.createBy);
        if (this.createdAt != null) {
            builder.append(",\n\tcreateAt: ").append(this.createdAt);
        }
        return builder.toString();
    }

//    @Override
//    public abstract String toString();

}
