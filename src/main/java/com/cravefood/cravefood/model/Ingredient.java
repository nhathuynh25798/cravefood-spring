package com.cravefood.cravefood.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "ingredient")
public class Ingredient extends AbstractModel {

    @Column(name = "unit")
    private String unit;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ingredient", cascade = CascadeType.REMOVE)
    private Set<DishIngredient> dishIngredients = new HashSet<>();

    public Ingredient() { super(); }

    public String getUnit() {
        return unit;
    }

    public Set<DishIngredient> getDishIngredients() {
        return dishIngredients;
    }

    public void setDishIngredients(Set<DishIngredient> dishIngredients) {
        this.dishIngredients = dishIngredients;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
