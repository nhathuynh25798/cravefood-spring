package com.cravefood.cravefood.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "category")
public class Category extends AbstractModel implements Serializable {

//    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
//    @OneToMany(fetch = FetchType.LAZY, mappedBy = "category", cascade = CascadeType.ALL)
//    private Set<ProductCategory> products = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(name = "product_category",
            joinColumns = { @JoinColumn(name = "category_id") },
            inverseJoinColumns = {@JoinColumn(name = "product_id") })
    private Set<Product> products = new HashSet<>();

    @Column(name = "slug")
    private String slug;

    @Column(name = "show_home")
    private Integer showHome;

    @Column(name = "show_menu")
    private Integer showMenu;

    public Category() {
        super();
    }

//    public Set<ProductCategory> getProducts() {
//        return products;
//    }
//
//    public void setProducts(Set<ProductCategory> products) {
//        this.products = products;
//    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Integer getShowHome() {
        return showHome;
    }

    public void setShowHome(Integer showHome) {
        this.showHome = showHome;
    }

    public Integer getShowMenu() {
        return showMenu;
    }

    public void setShowMenu(Integer showMenu) {
        this.showMenu = showMenu;
    }
}


