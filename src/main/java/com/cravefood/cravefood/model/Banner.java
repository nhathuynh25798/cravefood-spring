package com.cravefood.cravefood.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;

@Entity
@Table(name = "banner")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Banner extends AbstractModel{
}
