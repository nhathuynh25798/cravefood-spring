package com.cravefood.cravefood.model;

import com.cravefood.cravefood.annotation.Phone;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "address")
public class Address extends AbstractModel {

    @JsonProperty(value = "detail")
    @Column(name = "detail")
    private String detail;

    @JsonProperty(value = "province")
    @Column(name = "province")
    private String province;

    @JsonProperty(value = "district")
    @Column(name = "district")
    private String district;

    @JsonProperty(value = "ward")
    @Column(name = "ward")
    private String ward;

    @JsonProperty(value = "defaultAddressValue")
    @Column(name = "`default`")
    private Integer defaultAddress;

    @Phone
    @NotBlank(message = "Số điện thoại không được bỏ trống")
    @JsonProperty(value = "phone")
    @Column(name = "phone_contact")
    private String phoneContact;

    @JsonProperty(value = "note")
    @Column(name = "note")
    private String note;

    public Address() { super(); }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public Integer getDefaultAddress() {
        return defaultAddress;
    }

    public void setDefaultAddress(Integer defaultAddress) {
        this.defaultAddress = defaultAddress;
    }

    public String getPhoneContact() {
        return phoneContact;
    }

    public void setPhoneContact(String phoneContact) {
        this.phoneContact = phoneContact;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "Address{" +
                "detail='" + detail + '\'' +
                ", province='" + province + '\'' +
                ", district='" + district + '\'' +
                ", ward='" + ward + '\'' +
                ", defaultAddress=" + defaultAddress +
                ", phoneContact='" + phoneContact + '\'' +
                ", note='" + note + '\'' +
                '}';
    }
}
