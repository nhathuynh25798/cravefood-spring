package com.cravefood.cravefood.model;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "dish_size")
@IdClass(value = DishSize.DishSizeId.class)
@JsonSerialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DishSize implements Serializable {

    @Id
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id", nullable = false)
    private Product product;

    @Id
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "size_id", nullable = false)
    private Size size;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "dishSize", cascade = CascadeType.ALL)
    private Set<DishIngredient> ingredients = new HashSet<>();

    @Column(name = "price")
    private Double price;

    @Column(name = "enable")
    private Integer enable;

    public DishSize() {
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public Set<DishIngredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Set<DishIngredient> dishIngredients) {
        this.ingredients = dishIngredients;
    }

    public static class DishSizeId implements Serializable {

        private Product product;

        private Size size;

        public DishSizeId() {
            super();
        }

        public DishSizeId(Product product, Size size) {
            this.product = product;
            this.size = size;
        }

        public Product getProductId() {
            return product;
        }

        public void setProductId(Product product) {
            this.product = product;
        }

        public Size getSizeId() {
            return size;
        }

        public void setSizeId(Size size) {
            this.size = size;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof DishSizeId)) return false;
            DishSizeId that = (DishSizeId) o;
            return Objects.equals(product, that.product) && Objects.equals(size, that.size);
        }

        @Override
        public int hashCode() {
            return Objects.hash(product, size);
        }
    }
}
