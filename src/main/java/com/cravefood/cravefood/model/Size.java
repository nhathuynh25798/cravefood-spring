package com.cravefood.cravefood.model;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "`size`")
public class Size extends AbstractModel {

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "size", cascade = CascadeType.REMOVE)
    private Set<DishSize> dishSize = new HashSet<>();

    @Column(name = "image")
    private String image;

    public Size() { super(); }

    public Set<DishSize> getDishSize() {
        return dishSize;
    }

    public void setDishSize(Set<DishSize> dishSize) {
        this.dishSize = dishSize;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
