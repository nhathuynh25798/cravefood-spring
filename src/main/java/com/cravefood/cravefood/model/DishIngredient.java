package com.cravefood.cravefood.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@IdClass(value = DishIngredient.IngredientId.class)
@Table(name = "dish_ingredient")
@JsonSerialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DishIngredient implements Serializable {

    @JsonIgnore
    @Id
    @ManyToOne
    @JoinColumn(name = "ingredient_id", nullable = false)
    private Ingredient ingredient;

    @JsonIgnore
    @Id
    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    private Product product;

    @JsonIgnore
    @Id
    @ManyToOne
    @JoinColumn(name = "size_id", nullable = false)
    private Size size;

    @JsonIgnore
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "size_id", referencedColumnName = "size_id", nullable = false, insertable = false, updatable = false),
            @JoinColumn(name = "product_id", referencedColumnName = "product_id", nullable = false, insertable = false, updatable = false)
    })
    private DishSize dishSize;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "enable")
    private Integer enable;

    public DishIngredient() {
        super();
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public DishSize getDishSize() {
        return dishSize;
    }

    public void setDishSize(DishSize dishSize) {
        this.dishSize = dishSize;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public static class IngredientId implements Serializable {

        private Product product;

        private Size size;

        private Ingredient ingredient;

        public IngredientId() {
            super();
        }

        public IngredientId(Product product, Size size, Ingredient ingredient) {
            this.product = product;
            this.size = size;
            this.ingredient = ingredient;
        }


    }
}
