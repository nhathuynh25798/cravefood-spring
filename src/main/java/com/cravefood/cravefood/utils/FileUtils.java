package com.cravefood.cravefood.utils;

import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class FileUtils {

    public static final String IMAGE_FOLDER = "src/main/resources/static/images";

    public static void uploadFile(MultipartFile file, String name, String indentify) throws IOException {
        Path fileNamePath = Paths.get(new File(IMAGE_FOLDER).getAbsolutePath(),
                name.concat(indentify).concat(".").concat(Objects.requireNonNull(FilenameUtils.getExtension(file.getOriginalFilename()))));
        Files.write(fileNamePath, file.getBytes());
    }
}
