package com.cravefood.cravefood.utils;
import org.apache.commons.validator.routines.EmailValidator;

public class Validations {

    public static boolean isEmptyString(String string) {
        return string == null || string.trim().length() == 0 || string.equals("null");
    }

    public static boolean isInteger(String string) {
        try {
            Long.parseLong(string);
        }
        catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public static boolean isDataTooLong(String string, int max){
        return string.length() > max;
    }

    public boolean isObjectNull(Object o){
        return o == null;
    }


}
