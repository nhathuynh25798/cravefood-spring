package com.cravefood.cravefood.utils;

import java.io.*;

public class ExportSQL {

    public static void readFile(String source, String dest) throws IOException {
        String charset = "UTF-8";
        BufferedReader bReader = new BufferedReader(new InputStreamReader(new FileInputStream(source), charset));
        BufferedWriter bWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(dest), charset));
        System.out.println("======= Start =======");
        String line = "";
        while ((line = bReader.readLine()) != null) {
            String[] arr = line.split(",");
            String query = "insert into ward (name, create_by, enable, district_id) values (" + arr[1].trim() + ", 1, 1," + arr[4].trim() + ");";
            bWriter.write(query);
            bWriter.write("\n");
        }
        bWriter.close();
        bReader.close();
        System.out.println("======= Finish =======");
    }

    public static void main(String[] args) throws IOException {
        String source = "/home/nhathuynh/Documents/local/ward.txt";
        String dest = "/home/nhathuynh/Documents/local/ward.sql";

        readFile(source, dest);
    }
}
