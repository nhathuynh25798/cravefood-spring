package com.cravefood.cravefood.controller;

import com.cravefood.cravefood.service.DistrictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/district")
public class DistrictController extends AbstractController {

    private final DistrictService districtService;

    @Autowired
    public DistrictController(DistrictService districtService) {
        this.districtService = districtService;
    }

    @GetMapping(value = "/province/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List getDistrictsByProvinceId(@PathVariable Integer id) {
        return districtService.getDistrictsByProvinceId(id);
    }

    @GetMapping(value = {"/", "/list"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public List getDistrictById() {
        return districtService.getAllDistricts();
    }
}
