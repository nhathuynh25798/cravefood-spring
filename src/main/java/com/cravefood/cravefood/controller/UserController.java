package com.cravefood.cravefood.controller;

import com.cravefood.cravefood.dto.*;
import com.cravefood.cravefood.security.JwtUtils;
import com.cravefood.cravefood.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Objects;

@RestController
@RequestMapping("/user")
public class UserController extends AbstractController {

    private final UserService userService;
    private final JwtUtils jwtUtils;

    @Autowired
    public UserController(UserService userService, JwtUtils jwtUtils) {
        this.userService = userService;
        this.jwtUtils = jwtUtils;
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDTO getUserById(@PathVariable Integer id) {
        return UserDTO.build(userService.getUserById(id));
    }

    @GetMapping(value = {"/page/{page}", "/list/page/{page}"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseList<UserDTO> fetchUser(@PathVariable int page) {
        return new ResponseList<UserDTO>(userService.fetchUser(page), userService.getRowCount());
    }

    @PostMapping(value = "/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteUser(@PathVariable Integer id) {
        try {
            userService.deleteUser(id);
            return ResponseEntity.ok(new MessageResponse("Xoá thành công!"));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("DEFAULT", 401));
        }
    }

    @PostMapping(value = "/update")
    public ResponseEntity<?> updateUser(@RequestHeader("token") String token, @Valid @RequestBody UpdateUserRequestDTO updateUserRequestDTO,
                                        BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse(Objects.requireNonNull(Objects.requireNonNull(result.getFieldError()).getDefaultMessage()), 400));
        }
        try {
//            boolean isValid = jwtUtils.validateJwtToken(token);
//            if (isValid) {
            userService.updateUser(updateUserRequestDTO);
            return ResponseEntity.ok(new MessageResponse("Cập nhật thông tin thành công!"));
//            } else {
//                return ResponseEntity
//                        .badRequest()
//                        .body(new MessageResponse("Token is required", 401));
//            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("DEFAULT", 401));
        }
    }

    // <== WishList ==>
    @PostMapping(value = "/addWishList")
    public ResponseEntity<?> addWishList(@RequestHeader("token") String token, @RequestBody WishListRequest wishList) {
        try {
//            boolean isValid = jwtUtils.validateJwtToken(token);
//            if (isValid) {
            userService.addWishList(wishList);
            return ResponseEntity.ok(new MessageResponse("Thêm sản phẩm yêu thích thành công!"));
//            } else {
//                return ResponseEntity
//                        .badRequest()
//                        .body(new MessageResponse("Token is required", 401));
//            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("DEFAULT", 401));
        }
    }

    @PostMapping(value = "/deleteWishList")
    public ResponseEntity<?> deleteWishList(@RequestHeader("token") String token, @RequestBody WishListRequest wishList) {
        try {
//            boolean isValid = jwtUtils.validateJwtToken(token);
//            if (isValid) {
            userService.deleteWishList(wishList);
            return ResponseEntity.ok(new MessageResponse("Đã xoá khỏi sản phẩm yêu thích!"));
//            } else {
//                return ResponseEntity
//                        .badRequest()
//                        .body(new MessageResponse("Token is required", 401));
//            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("DEFAULT", 401));
        }
    }

    @PostMapping(value = "/wishlists")
    public ResponseEntity<?> getWithWish(@RequestHeader("token") String
                                                 token, @RequestHeader("uid") Integer id) {
        try {
//            boolean isValid = jwtUtils.validateJwtToken(token);
//            if (isValid) {
            return ResponseEntity.ok(userService.getWishList(id));
//            } else {
//                return ResponseEntity
//                        .badRequest()
//                        .body(new MessageResponse("Token is required", 401));
//            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("DEFAULT", 401));
        }
    }
    // <== WishList ==>

    // <== Order ==>
    @PostMapping(value = "/addOrder")
    public ResponseEntity<?> addOrder(@RequestHeader("token") String token, @RequestBody OrderRequest order) {
        try {
//            boolean isValid = jwtUtils.validateJwtToken(token);
//            if (isValid) {
            userService.addOrder(order);
            return ResponseEntity.ok(new MessageResponse("Đơn hàng đã được xác nhận!"));
//            } else {
//                return ResponseEntity
//                        .badRequest()
//                        .body(new MessageResponse("Token is required", 401));
//            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("DEFAULT", 401));
        }
    }

    @PostMapping(value = "/deleteOrder")
    public ResponseEntity<?> deleteOrder(@RequestHeader("token") String token, @RequestBody int id) {
        try {
//            boolean isValid = jwtUtils.validateJwtToken(token);
//            if (isValid) {
            userService.deleteOrder(id);
            return ResponseEntity.ok(new MessageResponse("Đã xoá đơn hàng thành công!"));
//            } else {
//                return ResponseEntity
//                        .badRequest()
//                        .body(new MessageResponse("Token is required", 401));
//            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("DEFAULT", 401));
        }
    }

    @PostMapping(value = "/orders")
    public ResponseEntity<?> getOrders(@RequestHeader("token") String
                                               token, @RequestHeader("uid") Integer id) {
        try {
//            boolean isValid = jwtUtils.validateJwtToken(token);
//            if (isValid) {
            return ResponseEntity.ok(userService.getOrders(id));
//            } else {
//                return ResponseEntity
//                        .badRequest()
//                        .body(new MessageResponse("Token is required", 401));
//            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("DEFAULT", 401));
        }
    }
    // <== Order ==>

    @PostMapping(value = "/getUser/{id}")
    public ResponseEntity<?> deleteWishList(@RequestHeader("token") String token, @PathVariable Integer id) {
        try {
//            boolean isValid = jwtUtils.validateJwtToken(token);
//            if (isValid) {
            return ResponseEntity.ok(userService.getUser(id));
//            } else {
//                return ResponseEntity
//                        .badRequest()
//                        .body(new MessageResponse("Token is required", 401));
//            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("DEFAULT", 401));
        }
    }

    // <== Address ==>
    @PostMapping(value = "/addAddress")
    public ResponseEntity<?> addAddress(@RequestHeader("token") String token, @RequestBody AddressRequest addresses) {
        try {
//            boolean isValid = jwtUtils.validateJwtToken(token);
//            if (isValid) {
            userService.addAddress(addresses.getAddresses());
            return ResponseEntity.ok(new MessageResponse("Thao tác thành công!"));
//            } else {
//                return ResponseEntity
//                        .badRequest()
//                        .body(new MessageResponse("Token is required", 401));
//            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("DEFAULT", 401));
        }
    }

    @PostMapping(value = "/deleteAddress/{id}")
    public ResponseEntity<?> deleteAddress(@RequestHeader("token") String token, @PathVariable Integer id) {
        try {
//            boolean isValid = jwtUtils.validateJwtToken(token);
//            if (isValid) {
            return userService.deleteAddress(id);
//            } else {
//                return ResponseEntity
//                        .badRequest()
//                        .body(new MessageResponse("Token is required", 401));
//            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("DEFAULT", 401));
        }
    }

    @PostMapping(value = "/addresses")
    public ResponseEntity<?> getAddress(@RequestHeader("token") String
                                                token, @RequestHeader("uid") Integer id) {
        try {
//            boolean isValid = jwtUtils.validateJwtToken(token);
//            if (isValid) {
            return ResponseEntity.ok(userService.getAddresses(id));
//            } else {
//                return ResponseEntity
//                        .badRequest()
//                        .body(new MessageResponse("Token is required", 401));
//            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("DEFAULT", 401));
        }
    }
    // <== Address ==>
}
