package com.cravefood.cravefood.controller;

import com.cravefood.cravefood.dto.MessageResponse;
import com.cravefood.cravefood.security.JwtUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

@RestController
@RequestMapping("/upload")
public class UploadController extends AbstractController {

    private final JwtUtils jwtUtils;

    @Autowired
    public UploadController(JwtUtils jwtUtils) {
        this.jwtUtils = jwtUtils;
    }

    public static final String IMAGE_FOLDER = "src/main/resources/static/images";

    @PostMapping(value = "/singleImage", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> uploadImage(@RequestHeader("token") String
                                                 token, @RequestParam("imageFile") MultipartFile file,
                                         @RequestParam("imageName") String name) {
        try {
            boolean isValid = jwtUtils.validateJwtToken(token);
            if (isValid) {
                uploadFile(file, name, "-image");
                return ResponseEntity.ok(new MessageResponse("Lưu ảnh thành công!"));
            }
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Token is required", 401));
        } catch (Exception exception) {
            exception.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("DEFAULT", 401));
        }
    }

    @PostMapping(value = "/multiImage", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> uploadMultiImage( @RequestParam("imageFiles") MultipartFile[] files,
                                              @RequestParam("imageName") String name) {
        try {
//            boolean isValid = jwtUtils.validateJwtToken(token);
//            if (isValid) {
                for (int i = 0; i < files.length; i++) {
                    uploadFile(files[i], name, Integer.toString(i));
                }
                return ResponseEntity.ok(new MessageResponse("Lưu ảnh thành công!"));
//            }
//            return ResponseEntity
//                    .badRequest()
//                    .body(new MessageResponse("Token is required", 401));
        } catch (Exception exception) {
            exception.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("DEFAULT", 401));
        }
    }

    public void uploadFile(MultipartFile file, String name, String indentify) throws IOException {
        Path fileNamePath = Paths.get(new File(IMAGE_FOLDER).getAbsolutePath(),
                name.concat(indentify).concat(".").concat(Objects.requireNonNull(FilenameUtils.getExtension(file.getOriginalFilename()))));
        Files.write(fileNamePath, file.getBytes());
    }

    @PostMapping(value = "deleteImage", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> deleteImage(@RequestHeader("token") String
                                                 token, @RequestParam("imageName") String fileName) {
        try {
//            boolean isValid = jwtUtils.validateJwtToken(token);
//            if (isValid) {
                Path fileNamePath = Paths.get(new File("src/main/resources/static").getAbsolutePath(),
                        fileName);
                Files.delete(fileNamePath);
                return ResponseEntity.ok(new MessageResponse("Xoá ảnh thành công!"));
//            }
//            return ResponseEntity
//                    .badRequest()
//                    .body(new MessageResponse("Token is required", 401));
        } catch (Exception exception) {
            exception.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("DEFAULT", 401));
        }
    }
}
