package com.cravefood.cravefood.controller;

import com.cravefood.cravefood.dto.*;
import com.cravefood.cravefood.model.Product;
import com.cravefood.cravefood.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController extends AbstractController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(value = {"/page/{page}", "/list/page/{page"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getProductsInPage(@PathVariable Integer page) {
        return ResponseEntity.ok(new ResponseList<Product>(productService.getAllProducts(page),productService.getCountRow()));
    }

    @GetMapping(value = "/{slug}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ProductDTO getProductBySlug(@PathVariable String slug) {
        return productService.getProductBySlug(slug);
    }

    @GetMapping(value = "/find/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getProductsById(@PathVariable String name) {
        if (!name.trim().equals("")) {
            return ResponseEntity.ok(productService.findByName(name));
        }
        return ResponseEntity
                .badRequest()
                .body(new MessageResponse("empty keyword"));
    }

    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> saveProduct(@RequestBody ProductDTO product) {
        try {
            return productService.saveProduct(product);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Lưu sản phẩm không thành công. Xin vui lòng thử lại.", 401));
        }
    }

    @PostMapping(value = "/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteProduct(@PathVariable Integer id) {
        try {
            productService.deleteProduct(id);
            return ResponseEntity.ok(new MessageResponse("Xoá thành công.", 200));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity
                .badRequest()
                .body(new MessageResponse("Xoá không thành công. Xin vui lòng thử lại.", 401));
    }

    @PostMapping(value = "/getNextId")
    public Integer getTheNextId() {
        return productService.getTheNextId();
    }

    @PostMapping(value = "/remove/dishSize")
    public ResponseEntity<?> removeDishSize(@RequestBody DishSizeNotIngredientsDTO dishSizeNotIngredientsDTO) {
        try {
        productService.deleteDishSize(dishSizeNotIngredientsDTO);
            return ResponseEntity.ok(new MessageResponse("Xoá thành công.", 200));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Xoá không thành công. Xin vui lòng thử lại.", 401));
        }
    }

    @PostMapping(value = "/remove/dishIngredient")
    public ResponseEntity<?> removeDishIngredient(@RequestBody DishIngredientRequest dishIngredientRequest) {
        try {
            productService.deleteDishIngredient(dishIngredientRequest);
            return ResponseEntity.ok(new MessageResponse("Xoá thành công.", 200));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Xoá không thành công. Xin vui lòng thử lại.", 401));
        }
    }
}

