package com.cravefood.cravefood.controller;

import com.cravefood.cravefood.service.ProvinceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/province")
public class ProvinceController extends AbstractController {

    private final ProvinceService provinceService;

    @Autowired
    public ProvinceController(ProvinceService provinceService) {
        this.provinceService = provinceService;
    }

    @GetMapping(value = {"/", "/list"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public List getProvincesById() {
        return provinceService.getAllProvince();
    }
}
