package com.cravefood.cravefood.controller;

import com.cravefood.cravefood.dto.BannerRequest;
import com.cravefood.cravefood.dto.MessageResponse;
import com.cravefood.cravefood.model.Banner;
import com.cravefood.cravefood.service.BannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/banner")
public class BannerController {

    private final BannerService bannerService;

    @Autowired
    public BannerController(BannerService bannerService) {
        this.bannerService = bannerService;
    }

    @GetMapping(value = {"/", "/list"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public List getBannerId() {
        return bannerService.getAllBanner();
    }

    @GetMapping(value = "/list-banner", produces = MediaType.APPLICATION_JSON_VALUE)
    public List getBannerAdmin() {
        return bannerService.getAllBannerAdmin();
    }

    @PostMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteBanner(@RequestBody Banner banner) {
        try {
            bannerService.deleteBanner(banner);
            return ResponseEntity.ok(new MessageResponse("Xoá thành công!"));
        } catch (IOException exception) {
            exception.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("DEFAULT", 401));
        }
    }

    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addBanner(@RequestBody BannerRequest banner) {
        try {
            bannerService.addBanner(banner);
            return ResponseEntity.ok(new MessageResponse("Thêm thành công!"));
        } catch (IOException exception) {
            exception.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("DEFAULT", 401));
        }
    }

    @PostMapping(value = "/update/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateBanner(@PathVariable Integer id) {
        try {
            bannerService.updateBanner(id);
            return ResponseEntity.ok(new MessageResponse("Ảnh này sẽ không hiển thị cho người dùng nữa"));
        } catch (Exception exception) {
            exception.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("DEFAULT", 401));
        }
    }
}
