package com.cravefood.cravefood.controller;

import com.cravefood.cravefood.dto.ResponseList;
import com.cravefood.cravefood.model.Ingredient;
import com.cravefood.cravefood.service.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ingredient")
public class IngredientController extends AbstractController {

    private final IngredientService ingredientService;

    @Autowired
    public IngredientController(IngredientService ingredientService) {
        this.ingredientService = ingredientService;
    }

    @GetMapping(value = {"/page/{page}", "/list/page/{page"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getIngredientInPage(@PathVariable Integer page) {
        return ResponseEntity.ok(new ResponseList<Ingredient>(ingredientService.getAllIngredient(page),ingredientService.getCountRow()));
    }
}
