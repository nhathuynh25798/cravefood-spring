package com.cravefood.cravefood.controller;

import com.cravefood.cravefood.dto.GlobalValue;
import com.cravefood.cravefood.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;

@RestController
@RequestMapping("/globalValue")
public class GlobalValueController extends AbstractController {

    private final BannerService bannerService;
    private final CategoryService categoryService;
    private final SizeService sizeService;
    private final IngredientService ingredientService;

    @Autowired
    public GlobalValueController(BannerService bannerService, CategoryService categoryService, SizeService sizeService, IngredientService ingredientService) {
        this.bannerService = bannerService;
        this.categoryService = categoryService;
        this.sizeService = sizeService;
        this.ingredientService = ingredientService;
    }

    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalValue getGlobalValues() {
        return new GlobalValue(
                new HashSet<>(categoryService.getCategoriesShowMenu()),
                new HashSet<>(categoryService.getCategoriesShowHome()),
                new HashSet<>(bannerService.getAllBanner()));
    }

    @GetMapping(value = "/admin", produces = MediaType.APPLICATION_JSON_VALUE)
    public AdminGlobalValue getAdminGlobalValues() {
        return new AdminGlobalValue(
                new HashSet<>(categoryService.adminGetCategories()),
                new HashSet<>(sizeService.getAllSize()));
    }
}
