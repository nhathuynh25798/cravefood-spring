package com.cravefood.cravefood.controller;

import com.cravefood.cravefood.dao.RoleDAO;
import com.cravefood.cravefood.dao.UserDAO;
import com.cravefood.cravefood.dto.*;
import com.cravefood.cravefood.model.Role;
import com.cravefood.cravefood.model.User;
import com.cravefood.cravefood.security.JwtUtils;
import com.cravefood.cravefood.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.jsonwebtoken.impl.DefaultClaims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class BaseAuthController {
    private final AuthenticationManager authenticationManager;

    private final UserDAO userDAO;
    private final RoleDAO roleDAO;
    private final UserService userService;
    private final PasswordEncoder encoder;
    private final JwtUtils jwtUtils;

    @Autowired
    public BaseAuthController(AuthenticationManager authenticationManager, UserDAO userDAO, RoleDAO roleDAO, PasswordEncoder encoder, JwtUtils jwtUtils,
                              UserService userService) {
        this.authenticationManager = authenticationManager;
        this.userDAO = userDAO;
        this.roleDAO = roleDAO;
        this.encoder = encoder;
        this.userService = userService;
        this.jwtUtils = jwtUtils;
    }

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) throws JsonProcessingException {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getEmail().trim(), loginRequest.getPassword().trim()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        return ResponseEntity.ok(new LoginResponse(jwt));
    }

    @PostMapping("/login/admin")
    public ResponseEntity<?> authenticateUserAdmin(@Valid @RequestBody LoginRequest loginRequest) throws JsonProcessingException {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getEmail().trim(), loginRequest.getPassword().trim()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtTokenForAdmin(authentication);
        if(jwt != null) {
            return ResponseEntity.ok(new LoginResponse(jwt));
        }
        return ResponseEntity
                .badRequest()
                .body(new MessageResponse("User is not found", 401));
    }

    @PostMapping("/register")
    @ResponseBody
    public ResponseEntity<?> registerUser(@Valid @RequestBody RegisterDTO signUpRequest, BindingResult result) throws JsonProcessingException {
        if (result.hasErrors()) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse(Objects.requireNonNull(Objects.requireNonNull(result.getFieldError()).getDefaultMessage()), 400));
        }
        if (userDAO.isEmailAddressUsed(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("email is already exist", 400));
        }
        // Create new user's account
        User user = new User(
                signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()));
        user.setPhoneNumber(signUpRequest.getPhone());
        Role usRole = roleDAO.getById(2);
        user.setRole(usRole);
        user.setEnable(1);
        userDAO.save(user);
        return ResponseEntity.ok(new MessageResponse("Chúc mừng bạn đã đăng ký tài khoản thành công!"));
    }

    @PostMapping(value = "/refreshToken")
    public ResponseEntity<?> refreshToken(HttpServletRequest request) throws Exception {
        // From the HttpRequest get the claims
        DefaultClaims claims = (io.jsonwebtoken.impl.DefaultClaims) request.getAttribute("claims");

        Map<String, Object> expectedMap = getMapFromIoJsonwebtokenClaims(claims);
        String token = jwtUtils.doGenerateRefreshToken(expectedMap, expectedMap.get("sub").toString());
        return ResponseEntity.ok(new LoginResponse(token));
    }

    public Map<String, Object> getMapFromIoJsonwebtokenClaims(DefaultClaims claims) {
        Map<String, Object> expectedMap = new HashMap<String, Object>();
        for (Map.Entry<String, Object> entry : claims.entrySet()) {
            expectedMap.put(entry.getKey(), entry.getValue());
        }
        return expectedMap;
    }
}
