package com.cravefood.cravefood.controller;

import com.cravefood.cravefood.dto.CategoriesDTO;
import com.cravefood.cravefood.dto.MessageResponse;
import com.cravefood.cravefood.dto.ProductsItemDTO;
import com.cravefood.cravefood.dto.ResponseList;
import com.cravefood.cravefood.model.Category;
import com.cravefood.cravefood.model.Product;
import com.cravefood.cravefood.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController extends AbstractController {

    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping(value = {"/page/{page}", "/list/page/{page"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getCategoriesInPage(@PathVariable Integer page) {
        return ResponseEntity.ok(new ResponseList<Category>(categoryService.getAllCategories(page), categoryService.getCountRow()));
    }

    @GetMapping(value = {"/", "/list"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getCategories() {
        return ResponseEntity.ok(categoryService.getAll());
    }

    @PostMapping(value = "/add", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addCategory(@RequestBody CategoriesDTO category) {
        return categoryService.addCategory(category);
    }

    @PostMapping(value = "/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteCategory(@PathVariable Integer id) {
        try {
            categoryService.deleteCategory(id);
            return ResponseEntity.ok(new MessageResponse("Xoá thành công.", 200));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity
                .badRequest()
                .body(new MessageResponse("Xoá không thành công. Xin vui lòng thử lại.", 401));
    }

    @GetMapping(value = "/{slug}/products", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProductsItemDTO> getProductByCategorySlug(@PathVariable String slug) {
        return categoryService.getProductsByCategorySlug(slug);
    }

    @GetMapping(value = "/{slug}", produces = MediaType.APPLICATION_JSON_VALUE)
    public CategoriesDTO getProductBySlugLimitProduct(@PathVariable String slug) {
        return categoryService.getCategoryBySlugLimitProduct(slug);
    }
}
