package com.cravefood.cravefood.controller;

import com.cravefood.cravefood.model.ProductCategory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository(value = "productCategoryDAO")
@Transactional(rollbackFor = Exception.class)
public class ProductCategoryDAO {

    @PersistenceContext
    public EntityManager entityManager;

    public void addProductCategory(ProductCategory model) {
        try {
            entityManager.merge(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeProductCategory(ProductCategory model) {
        try {
            entityManager.remove(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
