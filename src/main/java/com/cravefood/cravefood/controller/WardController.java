package com.cravefood.cravefood.controller;

import com.cravefood.cravefood.service.WardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/ward")
public class WardController extends AbstractController {

    private final WardService wardService;

    @Autowired
    public WardController(WardService wardService) {
        this.wardService = wardService;
    }

    @GetMapping(value = "/district/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List getWardsByDistrictId(@PathVariable Integer id) {
        return wardService.getWardsByDistrictId(id);
    }

    @GetMapping(value = {"/", "/list"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public List getWardsById() {
        return wardService.getAllWard();
    }
}
