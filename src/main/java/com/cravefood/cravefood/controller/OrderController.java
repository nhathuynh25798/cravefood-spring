package com.cravefood.cravefood.controller;

import com.cravefood.cravefood.dto.MessageResponse;
import com.cravefood.cravefood.dto.OrderDTO;
import com.cravefood.cravefood.dto.ResponseList;
import com.cravefood.cravefood.model.Ingredient;
import com.cravefood.cravefood.service.OrderService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
public class OrderController extends AbstractController {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping(value = {"/page/{page}", "/list/page/{page"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getIngredientInPage(@PathVariable Integer page) {
        return ResponseEntity.ok(new ResponseList<OrderDTO>(orderService.getOrders(page),orderService.getRowCount()));
    }

    @PostMapping(value = "/delete/{id}")
    public ResponseEntity<?> deleteOrder(@PathVariable int id) {
        try {
            orderService.deleteOrder(id);
            return ResponseEntity.ok(new MessageResponse("Xoá thành công.", 200));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Xoá không thành công. Xin vui lòng thử lại.", 401));
        }
    }
}
